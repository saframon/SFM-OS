
/* ----------------------------------------------------------------- *
 *
 *   gdt.h: declarations for the global descriptor table
 *          on x86 based systems.
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#ifndef KERNEL_ARCH_X86_GDT_HEADER
#define KERNEL_ARCH_X86_GDT_HEADER

#include <stdint.h>


#ifndef PACKED
#define PACKED __attribute__((packed))
#endif // PACKED


#define GDT_ENTRY_SIZE		sizeof(struct global_descriptor_table_entry)

#define GDT_ENTRY_NULL			0
#define GDT_ENTRY_KERNEL_CODE		1
#define GDT_ENTRY_KERNEL_DATA		2
#define GDT_ENTRY_USER_CODE		3
#define GDT_ENTRY_USER_DATA		4
#define GDT_NUM_ENTRIES			5

#define GDT_SELECTOR_NULL		GDT_ENTRY_NULL * GDT_ENTRY_SIZE
#define GDT_SELECTOR_KERNEL_CODE	GDT_ENTRY_KERNEL_CODE * GDT_ENTRY_SIZE
#define GDT_SELECTOR_KERNEL_DATA	GDT_ENTRY_KERNEL_DATA * GDT_ENTRY_SIZE
#define GDT_SELECTOR_USER_CODE		GDT_ENTRY_USER_CODE * GDT_ENTRY_SIZE
#define GDT_SELECTOR_USER_DATA		GDT_ENTRY_USER_DATA * GDT_ENTRY_SIZE

#define GDT_ACCESS			0b00010000
#define GDT_ACCESS_PRESENT		0b10000000
#define GDT_ACCESS_EXECUTABLE		0b00001000
#define GDT_ACCESS_DECREASING		0b00000100
#define GDT_ACCESS_READWRITE		0b00000010

#define GDT_ACCESS_RING0		0b00000000
#define GDT_ACCESS_RING1		0b00100000
#define GDT_ACCESS_RING2		0b01000000
#define GDT_ACCESS_RING3		0b01100000

#define GDT_FLAG_PAGE_GRANULARITY	0b1000
#define GDT_FLAG_32BIT			0b0100


struct global_descriptor_table_entry
{
	uint16_t limit_lo;
	uint16_t base_lo;
	uint8_t base_mi;
	uint8_t access;
	uint8_t flags;
	uint8_t base_hi;
} PACKED;


struct global_descriptor_table
{
	uint16_t limit;
	uint32_t base;
} PACKED;


void gdt_set ( uint32_t, uint32_t, uint32_t, uint8_t, uint8_t );
void gdt_flush ( void );
void gdt_init ( void );


extern struct global_descriptor_table kernel_gdtr;
extern struct global_descriptor_table_entry kernel_gdt[ GDT_NUM_ENTRIES ];


#endif // KERNEL_ARCH_X86_GDT_HEADER

