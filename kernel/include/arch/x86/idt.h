
/* ----------------------------------------------------------------- *
 *
 *   idt.h: declarations for the interrupt descriptor table
 *          on x86 based systems.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#ifndef KERNEL_ARCH_X86_IDT_HEADER
#define KERNEL_ARCH_X86_IDT_HEADER

#include <stdint.h>


#ifndef PACKED
#define PACKED __attribute__((packed))
#endif // PACKED


#define IDT_NUM_ENTRIES		256


#define IDT_ATTR_PRESENT	0b10000000
#define IDT_ATTR_TASKGATE	0b00010000

#define IDT_ATTR_RING0		0b00000000
#define IDT_ATTR_RING1		0b00100000
#define IDT_ATTR_RING2		0b01000000
#define IDT_ATTR_RING3		0b01100000

#define IDT_ATTR_TASK32		0b00000101
#define IDT_ATTR_INTERRUPT16	0b00000110
#define IDT_ATTR_TRAP16		0b00000111
#define IDT_ATTR_INTERRUPT32	0b00001110
#define IDT_ATTR_TRAP32		0b00001111


struct interrupt_descriptor_table_entry
{
	uint16_t base_lo;
	uint16_t selector;
	uint8_t zero;
	uint8_t attribute;
	uint16_t base_hi;
} PACKED;


struct interrupt_descriptor_table
{
	uint16_t limit;
	uint32_t base;
} PACKED;


void idt_set ( uint32_t, void (*)( void ), uint16_t, uint8_t );
void idt_flush ( void );
void idt_init ( void );


extern struct interrupt_descriptor_table kernel_idtr;
extern struct interrupt_descriptor_table_entry kernel_idt[ IDT_NUM_ENTRIES ];


#endif // KERNEL_ARCH_X86_IDT_HEADER

