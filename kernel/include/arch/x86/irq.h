
/* ----------------------------------------------------------------- *
 *
 *   irq.h: declarations for interrupt requests on x86
 *          based systems.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#ifndef KERNEL_ARCH_X86_IRQ_HEADER
#define KERNEL_ARCH_X86_IRQ_HEADER

#include <sys/system.h>

#include <stdint.h>


#define IRQ_NUM_HANDLERS	16

#define IRQ0	0x00
#define IRQ1	0x01
#define IRQ2	0x02
#define IRQ3	0x03
#define IRQ4	0x04
#define IRQ5	0x05
#define IRQ6	0x06
#define IRQ7	0x07
#define IRQ8	0x08
#define IRQ9	0x09
#define IRQ10	0x0a
#define IRQ11	0x0b
#define IRQ12	0x0c
#define IRQ13	0x0d
#define IRQ14	0x0e
#define IRQ15	0x0f

#define ICW1	0x11
#define ICW4	0x01

#define PICM_COMMAND_PORT0	0x20
#define PICM_COMMAND_PORT1	0x21
#define PICS_COMMAND_PORT0	0xa0
#define PICS_COMMAND_PORT1	0xa1


void irq_handler ( struct cpu_state* );
void irq_set ( uint32_t, void (*)( struct cpu_state* ) );
void irq_unset ( uint32_t );
void irq_init ( void );


extern void (*kernel_irqs[ IRQ_NUM_HANDLERS ])( struct cpu_state* );


#endif // KERNEL_ARCH_X86_IRQ_HEADER
