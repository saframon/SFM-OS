
/* ----------------------------------------------------------------- *
 *
 *   io.h: declarations and definitions for basic input/output
 *         operations of the system.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#ifndef KERNEL_SYS_IO_HEADER
#define KERNEL_SYS_IO_HEADER

#include <stdint.h>


uint8_t inbyte ( uint16_t );
uint16_t inword ( uint16_t );
uint32_t inlong ( uint16_t );

void outbyte ( uint8_t, uint16_t );
void outword ( uint16_t, uint16_t );
void outlong ( uint32_t, uint16_t );


#endif // KERNEL_SYS_IO_HEADER

