
/* ----------------------------------------------------------------- *
 *
 *   pageframe.h: declarations for the page frame handling
 *                of the kernel.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#ifndef KERNEL_SYS_PAGEFRAME_HEADER
#define KERNEL_SYS_PAGEFRAME_HEADER

#include <stddef.h>
#include <stdint.h>


#define PAGE_SIZE		(INT64_C(1)<<12) // 4096
#define MAX_PAGE_FRAMES		((INT64_C(1)<<32)/PAGE_SIZE) // 4GiB/PAGESIZE

#define BUDDY_ARRSIZE(x)	(((MAX_PAGE_FRAMES-1)/(1<<(x)))+1)


#define PAGEFRAME_FREE		0x00
#define PAGEFRAME_USED		0x01


struct pageframe_buddy
{
	int order;
	uint8_t* map;
};


extern const struct pageframe_buddy kernel_pageframe_buddies[];
extern const int kernel_pageframe_buddy_count;


void pageframe_init ( void );
int pageframe_allocate ( size_t );
int pageframe_deallocate ( int, size_t );


#endif // KERNEL_SYS_PAGEFRAME_HEADER

