
/* ----------------------------------------------------------------- *
 *
 *   meminfo.h: declarations for the memory info passed by
 *              the bootloader.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#ifndef KERNEL_SYS_MEMINFO_HEADER
#define KERNEL_SYS_MEMINFO_HEADER

#include <stdint.h>


#ifndef PACKED
#define PACKED		__attribute__((packed))
#endif // PACKED


struct memory_info
{
	uint32_t lower;
	uint32_t upper;
};


extern struct memory_info kernel_meminfo;


#endif // KERNEL_SYS_MEMINFO_HEADER


