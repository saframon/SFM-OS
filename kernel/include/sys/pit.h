
/* ----------------------------------------------------------------- *
 *
 *   pit.h: declarations for a programmable interval timer.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#ifndef KERNEL_SYS_PIT_HEADER
#define KERNEL_SYS_PIT_HEADER


#define PIT_INTERNAL_FREQUENCY	1193180

#define PIT_CHANNEL0_PORT	0x40
#define PIT_CHANNEL1_PORT	0x41
#define PIT_CHANNEL2_PORT	0x42
#define PIT_COMMAND_PORT	0x43

#define PIT_COMMAND_BYTE	0b00110110


void pit_init ( void );
void pit_set_frequency ( uint32_t );


#endif // KERNEL_SYS_PIT_HEADER

