
/* ----------------------------------------------------------------- *
 *
 *   multiboot.inc: general definitions for the implementation
 *                  of the multiboot specification. This file
 *                  only contains definitions relevant for the
 *                  multiboot header.
 *                  For more information, see:
 *
 *   https://www.gnu.org/software/grub/manual/multiboot2/multiboot.html
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


.ifndef	KERNEL_BOOT_MULTIBOOT_INCLUDE
.set	KERNEL_BOOT_MULTIBOOT_INCLUDE, 1


.set	MULTIBOOT_HEADER_MAGIC, 0xe85250d6

.set	MULTIBOOT_ARCH_I386, 0
.set	MULTIBOOT_ARCH_MIPS32, 4

.set	MULTIBOOT_HEADER_TAG_END, 0
.set	MULTIBOOT_HEADER_TAG_INFO_REQUEST, 1
.set	MULTIBOOT_HEADER_TAG_ADDRESS, 2
.set	MULTIBOOT_HEADER_TAG_ENTRY, 3
.set	MULTIBOOT_HEADER_TAG_CONSOLE_FLAGS, 4
.set	MULTIBOOT_HEADER_TAG_FRAMEBUFFER, 5
.set	MULTIBOOT_HEADER_TAG_MODULE_ALIGN, 6
.set	MULTIBOOT_HEADER_TAG_EFIBOOT, 7
.set	MULTIBOOT_HEADER_TAG_ENTRY_EFI32, 8
.set	MULTIBOOT_HEADER_TAG_ENTRY_EFI64, 9
.set	MULTIBOOT_HEADER_TAG_RELOCATABLE, 10

.set	MULTIBOOT_HEADER_FLAG_NONE, 0
.set	MULTIBOOT_HEADER_FLAG_OPTIONAL, 1

.set	MULTIBOOT_CONSOLE_FLAG_REQUIRED, 1
.set	MULTIBOOT_CONSOLE_FLAG_EGA, 2

.set	MULTIBOOT_RELOCATION_UNDEFINED, 0
.set	MULTIBOOT_RELOCATION_LOWEST, 1
.set	MULTIBOOT_RELOCATION_HIGHEST, 2

.set	MULTIBOOT_TAG_CMDLINE, 1
.set	MULTIBOOT_TAG_BOOTLOADER, 2
.set	MULTIBOOT_TAG_MODULE, 3
.set	MULTIBOOT_TAG_MEMORY_INFO, 4
.set	MULTIBOOT_TAG_BOOT_DEVICE, 5
.set	MULTIBOOT_TAG_MMAP, 6
.set	MULTIBOOT_TAG_VBE, 7
.set	MULTIBOOT_TAG_FRAMEBUFFER, 8
.set	MULTIBOOT_TAG_ELF_SECTIONS, 9
.set	MULTIBOOT_TAG_APM, 10
.set	MULTIBOOT_TAG_EFI32, 11
.set	MULTIBOOT_TAG_EFI64, 12
.set	MULTIBOOT_TAG_SMBIOS, 13
.set	MULTIBOOT_TAG_ACPI_OLD, 14
.set	MULTIBOOT_TAG_ACPI_NEW, 15
.set	MULTIBOOT_TAG_NETWORK, 16
.set	MULTIBOOT_TAG_EFI_MMAP, 17
.set	MULTIBOOT_TAG_EFI_NT, 18
.set	MULTIBOOT_TAG_EFI32_IH, 19
.set	MULTIBOOT_TAG_EFI64_IH, 20
.set	MULTIBOOT_TAG_LOAD_BASE, 21


.endif // KERNEL_BOOT_MULTIBOOT_INCLUDE


