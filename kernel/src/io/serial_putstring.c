
/* ----------------------------------------------------------------- *
 *
 *   serial_putstring.c: provides a function that writes a
 *                       NUL terminated string to the
 *                       serial port.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <io/output.h>
#include <io/serial.h>

#include <stddef.h>


void serial_putstring ( const char* s )
{
	// check whether the serial debug channel is active
	if ( __builtin_expect( !( kernel_active_debug_mode
		& DEBUG_CHANNEL_SERIAL ), FALSE ) )
	{
		// return
		return;
	}

	// iterate through the characters of the specified
	// string, and call `serial_putchar' for each
	// character seperately
	for ( char* c = (char*)( s ); *c; ++c )
	{
		// call `serial_putchar' to write this character
		// to the screen
		serial_putchar( *c );
	}
}

