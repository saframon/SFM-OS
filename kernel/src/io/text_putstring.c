
/* ----------------------------------------------------------------- *
 *
 *   text_putstring.c: provides a function that writes a
 *                     NUL terminated string to the textbuffer.
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <io/output.h>
#include <io/text.h>

#include <stddef.h>


void text_putstring ( const char* s )
{
	// check whether the serial debug channel is active
	if ( __builtin_expect( !( kernel_active_debug_mode
		& DEBUG_CHANNEL_TEXT ), FALSE ) )
	{
		// return
		return;
	}

	// iterate through the characters of the specified
	// string, and write each character seperately to the
	// screen
	for ( char* c = (char*)( s ); *c; ++c )
	{
		// call `text_putchar' to write this character
		// to the screen
		text_putchar( *c );
	}
}

