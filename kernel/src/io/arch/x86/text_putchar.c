
/* ----------------------------------------------------------------- *
 *
 *   text_putchar.c: provides a function that writes exactly
 *                   one character to the textbuffer.
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <io/output.h>
#include <io/text.h>
#include <sys/io.h>

#include <stddef.h>
#include <stdint.h>
#include <string.h>


extern const uint16_t* textbuffer_base;
extern const uint16_t* textbuffer_end;
extern const uint32_t textbuffer_linecount;
extern const uint32_t textbuffer_linewidth;
extern uint16_t* textbuffer_ptr;
extern uint8_t textbuffer_color;


static inline void handle_newline ( void )
{
	// in UNIX systems, the newline character means:
	// go down a line, but stay in the current column.
	// We will adopt that for our implementation
	textbuffer_ptr += textbuffer_linewidth;
}


static inline void handle_return ( void )
{
	// the RETURN character moves the cursor back
	// to the start of the line
	uintptr_t pos = (uintptr_t)( textbuffer_ptr )
			- (uintptr_t)( textbuffer_base );
	pos -= pos % ( textbuffer_linewidth * 2 );
	textbuffer_ptr = (uint16_t*)( pos + (uintptr_t)( textbuffer_base ) );
}


void text_putchar ( char c )
{
	// check whether the serial debug channel is active
	if ( __builtin_expect( !( kernel_active_debug_mode
		& DEBUG_CHANNEL_TEXT ), FALSE ) )
	{
		// return
		return;
	}

	// switch the character value to handle the
	// special characters below 0x20
	switch ( c )
	{
	case '\n':
		// it's the newline character
		handle_newline();
		__attribute__((fallthrough));
	case '\r':
		// it's the return character
		handle_return();
		break;
	default:
		// if it's none of the characters above,
		// just paste the character and the color value
		// into the text buffer, and then increase the
		// text buffer pointers
		*textbuffer_ptr++ = ( textbuffer_color << 8 ) | c;
		break;
	};

	// if we exceeded the buffer end, we have to 'scroll down a line'
	// In technical terms, move the buffer memory by 160 bytes (80*2)
	// and then we have space for another line in the buffer.
	while ( __builtin_expect( textbuffer_ptr >= textbuffer_end, FALSE ) )
	{
		// calculate how many characters we are able to keep
		size_t count = ( textbuffer_linecount - 1 )
			* textbuffer_linewidth;

		// use `memmove' from the standard library
		// to move the memory
		memmove( (void*)( textbuffer_base ),
			(void*)( textbuffer_base + textbuffer_linewidth ),
			count * 2 );

		// iterate through the last line and make sure all
		// characters are cleared
		for ( uint16_t* p = (uint16_t*)( textbuffer_base + count );
			p != (uint16_t*)( textbuffer_end ); ++p )
		{
			// the default value for a character slot
			// is 0x0F00
			*p = 0x0f00;
		}

		// and also we have to move the cursor up one line
		textbuffer_ptr -= textbuffer_linewidth;
	}

	// calculate the relative cursor location
	uint16_t v = textbuffer_ptr - textbuffer_base;

	// write the values into the corresponding ports
	outbyte( 0x0e, 0x03d4 );
	outbyte( ( v >> 8 ) & 0xFF, 0x03d5 );
	outbyte( 0x0f, 0x03d4 );
	outbyte( v & 0xFF, 0x03d5 );
}

