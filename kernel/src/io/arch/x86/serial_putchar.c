
/* ----------------------------------------------------------------- *
 *
 *   serial_putchar.c: provides a function that writes exactly
 *                     to the specified serial port.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <io/output.h>
#include <io/serial.h>
#include <sys/io.h>

#include <stdbool.h>
#include <stddef.h>


void serial_putchar ( char c )
{
	// check whether the serial debug channel is active
	if ( __builtin_expect( !( kernel_active_debug_mode
		& DEBUG_CHANNEL_SERIAL ), FALSE ) )
	{
		// return
		return;
	}

	// variables
	uint8_t t = UINT8_MAX;

	// wait until the line port is free
	while ( __builtin_expect( !( inbyte( kernel_serial_debug_port
		+ SERIAL_LNR_OFFSET ) & SERIAL_THRE_BIT ), TRUE ) )
	{
		// check whether we do still have tries left
		if ( __builtin_expect( !t, FALSE ) )
		{
			// return
			return;
		}

		// decrease `t'
		t -= 1;
	}

	// write the byte into the serial port
	outbyte( c, kernel_serial_debug_port + SERIAL_TXR_OFFSET );
}

