
/* ----------------------------------------------------------------- *
 *
 *   text.c: variables holding informations about the text
 *           output state, like the position of the cursor.
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <stdint.h>


#define TEXTBUFFER_BASE_ADDR		0x000b8000
#define TEXTBUFFER_LINE_COUNT		25
#define TEXTBUFFER_LINE_WIDTH		80
#define TEXTBUFFER_DEFAULT_COLOR	0x0f


const uint16_t* textbuffer_base = (const uint16_t*)( TEXTBUFFER_BASE_ADDR );
const uint16_t* textbuffer_end = (const uint16_t*)( TEXTBUFFER_BASE_ADDR )
	+ ( TEXTBUFFER_LINE_COUNT * TEXTBUFFER_LINE_WIDTH );

const uint32_t textbuffer_linecount = TEXTBUFFER_LINE_COUNT;
const uint32_t textbuffer_linewidth = TEXTBUFFER_LINE_WIDTH;

uint16_t* textbuffer_ptr = (uint16_t*)( TEXTBUFFER_BASE_ADDR );
uint8_t textbuffer_color = TEXTBUFFER_DEFAULT_COLOR;

