
/* ----------------------------------------------------------------- *
 *
 *   kernel_putstring.c: provides a function that writes a
 *                       NUL terminated string to all specified
 *                       debug channels.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <io/output.h>

#include <stddef.h>


void kernel_putstring ( const char* s )
{
	// check whether any debug channel is active at all
	if ( __builtin_expect( !( kernel_active_debug_mode
		& ( DEBUG_CHANNEL_SERIAL | DEBUG_CHANNEL_TEXT ) ), FALSE ) )
	{
		// return
		return;
	}

	// iterate through the characters of the specified
	// string, and call `kernel_putchar' for each character
	// separately
	for ( char* c = (char*)( s ); *c; ++c )
	{
		// push the current character
		kernel_putchar( *c );
	}
}

