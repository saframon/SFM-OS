
/* ----------------------------------------------------------------- *
 *
 *   kernel_putchar.c: provides a function that pushes a
 *                     single character to all specified
 *                     debug channels.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <io/output.h>
#include <io/serial.h>
#include <io/text.h>

#include <stddef.h>


void kernel_putchar ( char c )
{
	// check whether the serial debug channel is active
	if ( __builtin_expect( kernel_active_debug_mode
		& DEBUG_CHANNEL_SERIAL, TRUE ) )
	{
		// write the character to the serial debug channel
		serial_putchar( c );
	}

	// check whether the text debug channel is active
	if ( __builtin_expect( kernel_active_debug_mode
		& DEBUG_CHANNEL_TEXT, TRUE ) )
	{
		// write the character to the vga text channel
		text_putchar( c );
	}
}

