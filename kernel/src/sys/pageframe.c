
/* ----------------------------------------------------------------- *
 *
 *   pageframe.c: variables for handling the page frames
 *                of the kernel.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <sys/pageframe.h>

#include <stdint.h>


static uint8_t buddy_order0_map[ BUDDY_ARRSIZE(0) ];
static uint8_t buddy_order1_map[ BUDDY_ARRSIZE(1) ];
static uint8_t buddy_order2_map[ BUDDY_ARRSIZE(2) ];
static uint8_t buddy_order3_map[ BUDDY_ARRSIZE(3) ];
static uint8_t buddy_order4_map[ BUDDY_ARRSIZE(4) ];
//static uint8_t buddy_order5_map[ BUDDY_ARRSIZE(5) ];
static uint8_t buddy_order6_map[ BUDDY_ARRSIZE(6) ];
//static uint8_t buddy_order7_map[ BUDDY_ARRSIZE(7) ];
static uint8_t buddy_order8_map[ BUDDY_ARRSIZE(8) ];
//static uint8_t buddy_order9_map[ BUDDY_ARRSIZE(9) ];
//static uint8_t buddy_order10_map[ BUDDY_ARRSIZE(10) ];
//static uint8_t buddy_order11_map[ BUDDY_ARRSIZE(11) ];
static uint8_t buddy_order12_map[ BUDDY_ARRSIZE(12) ];
//static uint8_t buddy_order13_map[ BUDDY_ARRSIZE(13) ];
//static uint8_t buddy_order14_map[ BUDDY_ARRSIZE(14) ];
static uint8_t buddy_order15_map[ BUDDY_ARRSIZE(15) ];


const struct pageframe_buddy kernel_pageframe_buddies[] = 
{
	// NOTE: THE FIRST ENTRY MUST BE THE ORDER 0 ENTRY.
	{
		.order = 0,
		.map = buddy_order0_map,
	},
	// NOTE: THE OtHER ENTRY ARE OPTIONAL AND ARE ONLY
	//       REQUIRED TO BE SORTED BY THEIR ORDER
	{
		.order = 1,
		.map = buddy_order1_map,
	},
	{
		.order = 2,
		.map = buddy_order2_map,
	},
	{
		.order = 3,
		.map = buddy_order3_map,
	},
	{
		.order = 4,
		.map = buddy_order4_map,
	},
//	{
//		.order = 5,
//		.map = buddy_order5_map,
//	},
	{
		.order = 6,
		.map = buddy_order6_map,
	},
//	{
//		.order = 7,
//		.map = buddy_order7_map,
//	},
	{
		.order = 8,
		.map = buddy_order8_map,
	},
//	{
//		.order = 9,
//		.map = buddy_order9_map,
//	},
//	{
//		.order = 10,
//		.map = buddy_order10_map,
//	},
//	{
//		.order = 11,
//		.map = buddy_order11_map,
//	},
	{
		.order = 12,
		.map = buddy_order12_map,
	},
//	{
//		.order = 13,
//		.map = buddy_order13_map,
//	},
//	{
//		.order = 14,
//		.map = buddy_order14_map,
//	},
	{
		.order = 15,
		.map = buddy_order15_map,
	},
};


const int kernel_pageframe_buddy_count = 
	sizeof(kernel_pageframe_buddies) / sizeof(struct pageframe_buddy);

