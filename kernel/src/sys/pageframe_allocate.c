
/* ----------------------------------------------------------------- *
 *
 *   pageframe_allocate.c: allocates the specified number of
 *                         consecutive page frames.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <sys/pageframe.h>

#include <stdbool.h>
#include <stddef.h>


static void pageframe_mark_used ( int page, size_t num )
{
	// iterate through all pageframe buddies
	for ( int i = 0; i != kernel_pageframe_buddy_count; ++i )
	{
		// retrieve the buddy parameters
		int o = kernel_pageframe_buddies[ i ].order;
		uint8_t* a = kernel_pageframe_buddies[ i ].map;

		// get the first aligned pageframe address
		int _page = page & -( (1<<o) );

		// iterate through all buddy entries important
		// for the requested pageframe block
		for ( int j = _page; j < (int)( page + num ); j += (1<<o) )
		{
			// mark that entry as used
			a[ j / (1<<o) ] = PAGEFRAME_USED;
		}
	}
}


int pageframe_allocate ( size_t num )
{
	// check if there is at least one page requested
	if ( __builtin_expect( num == 0, FALSE ) )
	{
		// early return
		return -1;
	}

	// variables
	int order = 0;

	// get the order of the requested amount of pages
	for ( int x = num - 1; x > 0; x >>= 1 )
	{
		// increase the order
		++order;
	}

	// check if the requested block size is actually bigger
	// than than it's order
	if ( __builtin_expect( (int)( num ) > (1<<order), TRUE ) )
	{
		// if so, increase the order by 1
		++order;
	}

	// iterate through all pageframe buddies
	for ( int i = 0; i != kernel_pageframe_buddy_count; ++i )
	{
		// check whether the order is big enough
		if ( __builtin_expect(
			kernel_pageframe_buddies[ i ].order < order, TRUE ) )
		{
			// continue
			continue;
		}

		// retrieve the buddy parameters
		int o = kernel_pageframe_buddies[ i ].order;
		uint8_t* m = kernel_pageframe_buddies[ i ].map;

		// loop through the buddy map
		for ( int j = 0; j != BUDDY_ARRSIZE( o ); ++j )
		{
			// check whether we found a free pageframe block
			if ( __builtin_expect(
				m[ j ] != PAGEFRAME_FREE, TRUE ) )
			{
				// continue
				continue;
			}

			// get the index of the first pageframe
			// of the block
			int p = j * (1<<o);

			// mark that pageframe block as used
			pageframe_mark_used( p, (1<<o) );

			// return that page
			return p;
		}

		// no free pageframe block of the requested
		// size available
		return -1;
	}

	// no buddy that is big enough is available
	return -1;
}

