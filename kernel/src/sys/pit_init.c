
/* ----------------------------------------------------------------- *
 *
 *   pit_init.c: initialize the programmable interrupt timer
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <arch/x86/irq.h>
#include <io/serial.h>
#include <sys/pit.h>
#include <sys/system.h>

#include <stdint.h>


#ifndef UNUSED
#define UNUSED		__attribute__((unused))
#endif // UNUSED


void pit_callback ( UNUSED struct cpu_state* state )
{
	// print a dot
	serial_putchar( '.' );

	// return
	return;
}


void pit_init ( void )
{
	// all we have to do is to install the callback
	// function into the IRQ table at slot 0
	irq_set( IRQ0, pit_callback );
}

