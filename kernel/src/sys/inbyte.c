
/* ----------------------------------------------------------------- *
 *
 *   inbyte.c: reads a single byte from the specified port.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <sys/io.h>

#include <stdint.h>



uint8_t inbyte ( uint16_t port )
{
	// temporary variable
	uint8_t value;

	// just use the x86 `inb' instruction
	__asm__ __volatile__ ( "inb %w1, %b0" : "=a"(value) : "Nd"(port) );

	// return
	return value;
}

