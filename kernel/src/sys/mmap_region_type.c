
/* ----------------------------------------------------------------- *
 *
 *   mmap_region_type.c: search for the maximum type of an
 *                       MMAP entry interferring with the
 *                       specified memory region.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <sys/mmap.h>

#include <stddef.h>
#include <stdint.h>


uint32_t mmap_region_type ( uint64_t base, uint64_t length )
{
	// create the variable for the value we will return
	// at the end of the function
	uint32_t t = MMAP_TYPE_UNDEFINED;

	// iterate over all current entries in the MMAP
	// structure
	for ( struct mmap_entry* p = kernel_mmap.start;
		p < kernel_mmap.end; ++p )
	{
		// check if we can savely stop looping, as
		// no more entries of importance for us
		// will come
		if ( __builtin_expect( p->base >= base + length, FALSE ) )
		{
			// break out of the loop
			break;
		}

		// check if either the base or the end of the
		// are within the bounds specified
		if ( __builtin_expect( base <= p->base
			|| ( p->base < base && base < p->base + p->length ),
			FALSE ) )
		{
			// check the type of entry against
			// `undefined'
			if ( __builtin_expect(
				p->type == MMAP_TYPE_UNDEFINED, FALSE ) )
			{
				// set `t' to undefined and break
				// out of the loop
				t = MMAP_TYPE_UNDEFINED;
				break;
			}

			// else, check whether the type is greater
			// than the one stored in `t'
			if ( __builtin_expect( p->type > t, FALSE ) )
			{
				// update `t'
				t = p->type;
			}
		}
	}

	// return `t'
	return t;
}


