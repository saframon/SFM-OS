
/* ----------------------------------------------------------------- *
 *
 *   pit_set_frequency.c: a function to set the frequency
 *                        of the programmable interval timer.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <sys/io.h>
#include <sys/pit.h>

#include <stdint.h>


void pit_set_frequency ( uint32_t freq )
{
	// signalize the cpu we want to change the
	// PIT operation frequency
	outbyte( PIT_COMMAND_BYTE, PIT_COMMAND_PORT );

	// calculate how many cycles of the input clock
	// should represent a tick of the timer
	uint16_t cycles = PIT_INTERNAL_FREQUENCY / freq;

	// send the calculated information to the cpu
	outbyte( (uint8_t)( cycles & 0xFF ), PIT_CHANNEL0_PORT );
	outbyte( (uint8_t)( cycles >> 8 ), PIT_CHANNEL0_PORT );
}

