
/* ----------------------------------------------------------------- *
 *
 *   mmap_print.c: prints out the MMAP structure of the kernel.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <io/output.h>
#include <sys/mmap.h>

#include <stddef.h>


static const char* mmap_type_to_string ( uint32_t type )
{
	// switch the type value
	switch ( type )
	{
	case MMAP_TYPE_UNDEFINED:
		return "undefined";
	case MMAP_TYPE_FREE:
		return "usable";
	case MMAP_TYPE_RESERVED:
		return "reserved";
	case MMAP_TYPE_ACPI:
		return "ACPI reclaimable";
	case MMAP_TYPE_NVS:
		return "ACPI NVS";
	case MMAP_TYPE_BADMEM:
		return "bad memory";
	default:
		return "undefined";
	}
}


void mmap_print ( const char* prefix )
{
#ifdef KERNEL_SAFETY
	// check whether the struture is marked sanitized
	if ( __builtin_expect( !kernel_mmap.sanitized, FALSE ) )
	{
		// print a note that the MMAP structure might
		// not be sanitized
		kernel_printf( "%s [structure might not be sanitized]\n",
			prefix );
	}
#endif // KERNEL_SAFETY

	// iterate over all entries in the structure
	for ( struct mmap_entry* p = kernel_mmap.start;
		p != kernel_mmap.end; ++p )
	{
		// print the current entry to the screen
		kernel_printf( "%s %016llx %016llx [%s]\n", prefix,
			p->base, p->base + p->length - 1,
			mmap_type_to_string( p->type ) );
	}
}

