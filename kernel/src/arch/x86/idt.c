
/* ----------------------------------------------------------------- *
 *
 *   idt.c: global variable holder for the interrupt descriptor
 *          table on x86 based systems.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <arch/x86/idt.h>


struct interrupt_descriptor_table kernel_idtr;
struct interrupt_descriptor_table_entry kernel_idt[ IDT_NUM_ENTRIES ];

