
/* ----------------------------------------------------------------- *
 *
 *   irq_stub.s: defines the interrupt request stubs for all
 *               16 interrupt requests.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


.extern irq_handler


.code32
.org 0x00

.section .text

.type irq_stub_common, @function

irq_stub_common:
	// -----------------------
	// |   IRQ_STUB_COMMON   |
	// -----------------------

	// push all registers
	pusha

	// save the segment registers to the stack
	push	%ds
	push	%es
	push	%fs
	push	%gs

	// set the segment register to 0x10, the kernel
	// data segment
	mov	$0x10, %eax
	mov	%eax, %ds
	mov	%eax, %es
	mov	%eax, %fs
	mov	%eax, %gs

	// make sure the direction flag is cleared
	cld

	// call the interrupt request handler
	mov	%esp, %eax
	push	%eax
	call	irq_handler
	add	$4, %esp

	// restore the segment registers from the stack
	pop	%gs
	pop	%fs
	pop	%es
	pop	%ds

	// restore the rest of the registers
	popa

	// pop the previously pushed error code and
	// interrupt index
	add	$8, %esp

	// return
	iret



.macro IRQ_STUB num index
.global irq_stub\num
.type irq\num, @function
irq_stub\num:
	cli
	push	$0x00
	push	$\index
	jmp	irq_stub_common
.endm


IRQ_STUB 0, 32
IRQ_STUB 1, 33
IRQ_STUB 2, 34
IRQ_STUB 3, 35
IRQ_STUB 4, 36
IRQ_STUB 5, 37
IRQ_STUB 6, 38
IRQ_STUB 7, 39
IRQ_STUB 8, 40
IRQ_STUB 9, 41
IRQ_STUB 10, 42
IRQ_STUB 11, 43
IRQ_STUB 12, 44
IRQ_STUB 13, 45
IRQ_STUB 14, 46
IRQ_STUB 15, 47

