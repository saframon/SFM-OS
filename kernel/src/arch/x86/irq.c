
/* ----------------------------------------------------------------- *
 *
 *   irq.c: global variable holder for interrupt requests
 *          on x86 based systems.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <arch/x86/irq.h>
#include <sys/system.h>

#include <stddef.h>


void (*kernel_irqs[ IRQ_NUM_HANDLERS ])( struct cpu_state* );

