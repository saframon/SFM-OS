
/* ----------------------------------------------------------------- *
 *
 *   isr.c: global variable holder for interrupt service
 *          routines on x86 based systems.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <arch/x86/isr.h>
#include <sys/system.h>

#include <stddef.h>


void (*kernel_isrs[ ISR_NUM_HANDLERS ])( struct cpu_state* );

