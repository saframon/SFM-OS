
/* ----------------------------------------------------------------- *
 *
 *   idt_flush.s: flushes the old interrupt descriptor table
 *                and loads a new one into the cpu.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


.extern kernel_idtr

.code32
.org 0x00

.section .text

.global idt_flush
.type idt_flush, @function

idt_flush:
	// -------------------
	// |   IDT_FLUSH()   |
	// -------------------

	// load the kernel idt structure
	lidt	kernel_idtr

	// return
	ret

