
/* ----------------------------------------------------------------- *
 *
 *   kernel_start.s: provides the entry point for the kernel
 *                   on x86 architectures.
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


.extern kernel_init
.extern kernel_main
.extern kernel_printf


.code32
.org 0x00

.section .text

.global _start
.type _start, @function

_start:
	// --------------------
	// |   KERNEL START   |
	// --------------------

	// ensure interrupts are disabled for the moment.
	// The kernel will enable them in a later stage.
	cli

	// update the stack pointer to point to the provided
	// kernel stack space
	mov	$kernel_stack.top, %esp

	// now call the kernel initialization function
	// to prepare the kernel for us
	push	%ebx		/* param: mbi */
	push	%eax		/* param: magic */
	call	kernel_init
	add	$8, %esp
	test	%eax, %eax	/* 0 means success, else failure */
	jnz	kernel_start.halt

	// now that the system is correctly initialized,
	// we can enable interrupts
	sti

	// now, jump into the main kernel function to
	// actually handle all the kernel stuff
	jmp	kernel_main

kernel_start.halt:
	// we should only return here when a error has occured.
	// We will just print a message containing the
	// return value of the kmain() call above.
	push	%eax				/* param: retval */
	push	$kernel_start.halt.message	/* param: fmt */
	call	kernel_printf
	add	$8, %esp

kernel_start.halt.loop:
	// now just halt the system. There is nothing
	// else we can do
	cli
	hlt

	// loop, just to be 1000% sure we don't continue
	// executing code
	jmp	kernel_start.halt.loop
	
kernel_start.halt.message:
	// the message format string that will be printed in
	// case the kmain() call returns with an error.
	.string	"SYSTEM HALTED! [ERROR CODE %i]"



.section .bss

kernel_stack:
	// --------------------
	// |   KERNEL STACK   |
	// --------------------

	// initialize the kernel stack with the specified
	// amount of space. The default space is 4KiB large.
	.skip	4096

kernel_stack.top:

