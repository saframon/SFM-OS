
/* ----------------------------------------------------------------- *
 *
 *   gdt.c: global variable holder for the global descriptor
 *          table on x86 based systems.
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <arch/x86/gdt.h>


struct global_descriptor_table kernel_gdtr;
struct global_descriptor_table_entry kernel_gdt[ GDT_NUM_ENTRIES ];

