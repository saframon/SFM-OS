
/* ----------------------------------------------------------------- *
 *
 *   gdt_flush.s: flushes the old global descriptor table
 *                and loads a new one into the cpu.
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


.extern kernel_gdtr

.code32
.org 0x00

.section .text

.global gdt_flush
.type gdt_flush, @function

gdt_flush:
	// -------------------
	// |   GDT_FLUSH()   |
	// -------------------

	// load the kernel gdt structure
	lgdt	kernel_gdtr

	// do a far jump into the next line, so the code
	// segment gets updated
	ljmp	$0x08, $gdt_flush.next

gdt_flush.next:
	// let all segment registers - except the code segment
	// register - point to the kernel data selector, which
	// in our case is 0x10
	mov	$0x10, %eax
	mov	%eax, %ds
	mov	%eax, %es
	mov	%eax, %fs
	mov	%eax, %gs
	mov	%eax, %ss

	// return
	ret

