
/* ----------------------------------------------------------------- *
 *
 *   gdt_set.c: installs a global descriptor table entry
 *              into the global descriptor table structure
 *              of the kernel.
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <arch/x86/gdt.h>

#include <stddef.h>
#include <stdint.h>


void gdt_set ( uint32_t index, uint32_t base,
	uint32_t limit, uint8_t access, uint8_t flags )
{
	// test whether a valid index has been passed
	if ( __builtin_expect( index >= GDT_NUM_ENTRIES, FALSE ) )
	{
		// if not, we should immediately return
		return;
	}

	// write all values into their corresponding field
	// in the global descriptor entry structure
	kernel_gdt[ index ].limit_lo = limit & 0xFFFF;
	kernel_gdt[ index ].base_lo = base & 0xFFFF;
	kernel_gdt[ index ].base_mi = ( base >> 16 ) & 0xFF;
	kernel_gdt[ index ].access = access;
	kernel_gdt[ index ].flags |= ( limit >> 16 ) & 0x0F;
	kernel_gdt[ index ].flags = ( flags << 4 ) & 0xF0;
	kernel_gdt[ index ].base_hi = ( base >> 24 ) & 0xFF;
}

