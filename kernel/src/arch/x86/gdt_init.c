
/* ----------------------------------------------------------------- *
 *
 *   gdt_init.c: initializes the global descriptor table
 *               for the kernel.
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <arch/x86/gdt.h>

#include <stddef.h>


void gdt_init ( void )
{
	// setup the global descriptor table structure
	kernel_gdtr.limit = sizeof(kernel_gdt) - 1;
	kernel_gdtr.base = (uint32_t)( kernel_gdt );

	// set the NULL descriptor (selector=0x00)
	gdt_set( GDT_ENTRY_NULL, 0x00000000, 0x00000000, GDT_ACCESS, 0x00 );

	// set the kernel code segment descriptor (with selector 0x08)
	gdt_set( GDT_ENTRY_KERNEL_CODE, 0x00000000, 0x000fffff,
		GDT_ACCESS | GDT_ACCESS_PRESENT | GDT_ACCESS_RING0
		| GDT_ACCESS_EXECUTABLE | GDT_ACCESS_READWRITE,
		GDT_FLAG_PAGE_GRANULARITY | GDT_FLAG_32BIT );

	// set the kernel data segment descriptor (with selector 0x10)
	gdt_set( GDT_ENTRY_KERNEL_DATA, 0x00000000, 0x000fffff,
		GDT_ACCESS | GDT_ACCESS_PRESENT | GDT_ACCESS_RING0
		| GDT_ACCESS_READWRITE,
		GDT_FLAG_PAGE_GRANULARITY | GDT_FLAG_32BIT );

	// set the user code segment descriptor (with selector 0x18)
	gdt_set( GDT_ENTRY_USER_CODE, 0x00000000, 0x000fffff,
		GDT_ACCESS | GDT_ACCESS_PRESENT | GDT_ACCESS_RING3
		| GDT_ACCESS_EXECUTABLE | GDT_ACCESS_READWRITE,
		GDT_FLAG_PAGE_GRANULARITY | GDT_FLAG_32BIT );

	// set the user data segment descriptor (with selector 0x20)
	gdt_set( GDT_ENTRY_USER_DATA, 0x00000000, 0x000fffff,
		GDT_ACCESS | GDT_ACCESS_PRESENT | GDT_ACCESS_RING3
		| GDT_ACCESS_READWRITE,
		GDT_FLAG_PAGE_GRANULARITY | GDT_FLAG_32BIT );

	// flush the old descriptor entries and load
	// the new ones into the cpu
	gdt_flush();
}

