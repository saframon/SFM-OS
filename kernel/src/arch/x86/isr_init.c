
/* ----------------------------------------------------------------- *
 *
 *   isr_init.c: initializes the interrupt service routines
 *               for the kernel.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the SFM-OS kernel, and is licensed
 *   under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <arch/x86/gdt.h>
#include <arch/x86/idt.h>
#include <arch/x86/isr.h>
#include <sys/io.h>

#include <stddef.h>
#include <string.h>


extern void isr_stub0 ( void );
extern void isr_stub1 ( void );
extern void isr_stub2 ( void );
extern void isr_stub3 ( void );
extern void isr_stub4 ( void );
extern void isr_stub5 ( void );
extern void isr_stub6 ( void );
extern void isr_stub7 ( void );
extern void isr_stub8 ( void );
extern void isr_stub9 ( void );
extern void isr_stub10 ( void );
extern void isr_stub11 ( void );
extern void isr_stub12 ( void );
extern void isr_stub13 ( void );
extern void isr_stub14 ( void );
extern void isr_stub15 ( void );
extern void isr_stub16 ( void );
extern void isr_stub17 ( void );
extern void isr_stub18 ( void );
extern void isr_stub19 ( void );
extern void isr_stub20 ( void );
extern void isr_stub21 ( void );
extern void isr_stub22 ( void );
extern void isr_stub23 ( void );
extern void isr_stub24 ( void );
extern void isr_stub25 ( void );
extern void isr_stub26 ( void );
extern void isr_stub27 ( void );
extern void isr_stub28 ( void );
extern void isr_stub29 ( void );
extern void isr_stub30 ( void );
extern void isr_stub31 ( void );


void isr_init ( void )
{
	// initialize all entries with NULL
	memset( (void*)( kernel_isrs ), 0x00, sizeof(kernel_isrs) );

	// insert ISR0 into the idt table
	idt_set( ISR0, isr_stub0,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR1 into the idt table
	idt_set( ISR1, isr_stub1,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR2 into the idt table
	idt_set( ISR2, isr_stub2,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR3 into the idt table
	idt_set( ISR3, isr_stub3,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR4 into the idt table
	idt_set( ISR4, isr_stub4,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR5 into the idt table
	idt_set( ISR5, isr_stub5,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR6 into the idt table
	idt_set( ISR6, isr_stub6,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR7 into the idt table
	idt_set( ISR7, isr_stub7,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR8 into the idt table
	idt_set( ISR8, isr_stub8,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR9 into the idt table
	idt_set( ISR9, isr_stub9,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR10 into the idt table
	idt_set( ISR10, isr_stub10,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR11 into the idt table
	idt_set( ISR11, isr_stub11,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR12 into the idt table
	idt_set( ISR12, isr_stub12,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR13 into the idt table
	idt_set( ISR13, isr_stub13,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR14 into the idt table
	idt_set( ISR14, isr_stub14,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR15 into the idt table
	idt_set( ISR15, isr_stub15,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR16 into the idt table
	idt_set( ISR16, isr_stub16,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR17 into the idt table
	idt_set( ISR17, isr_stub17,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR18 into the idt table
	idt_set( ISR18, isr_stub18,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR19 into the idt table
	idt_set( ISR19, isr_stub19,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR20 into the idt table
	idt_set( ISR20, isr_stub20,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR21 into the idt table
	idt_set( ISR21, isr_stub21,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR22 into the idt table
	idt_set( ISR22, isr_stub22,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR23 into the idt table
	idt_set( ISR23, isr_stub23,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR24 into the idt table
	idt_set( ISR24, isr_stub24,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR25 into the idt table
	idt_set( ISR25, isr_stub25,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR26 into the idt table
	idt_set( ISR26, isr_stub26,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR27 into the idt table
	idt_set( ISR27, isr_stub27,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR28 into the idt table
	idt_set( ISR28, isr_stub28,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR29 into the idt table
	idt_set( ISR29, isr_stub29,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR30 into the idt table
	idt_set( ISR30, isr_stub30,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );

	// insert ISR31 into the idt table
	idt_set( ISR31, isr_stub31,
		GDT_SELECTOR_KERNEL_CODE,
		IDT_ATTR_PRESENT | IDT_ATTR_RING0 | IDT_ATTR_INTERRUPT32 );
}

