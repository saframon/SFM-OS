
/* ----------------------------------------------------------------- *
 *
 *   stdlib.h: basic function declarations for the C standard
 *             library.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the C standard library for the
 *   SFM-OS kernel, and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


#ifndef LIBSTD_STDLIB_HEADER
#define LIBSTD_STDLIB_HEADER

#include <stdint.h>


#ifndef NORETURN
#define NORETURN	__attribute__((noreturn))
#endif // NORETURN

#ifndef MALLOC
#define MALLOC		__attribute__((malloc))
#endif // MALLOC


#define NULL			(void*)( 0x00000000 )
#define	EXIT_FAILURE		0x01
#define	EXIT_SUCCESS		0x00
#define RAND_MAX		INT32_MAX
#define MB_CUR_MAX


#define abs(x)		( ( (x) > 0 ) ? (x) : -(x) )
#define labs(x)		abs( x )


typedef __SIZE_TYPE__ size_t;
typedef __WCHAR_TYPE__ wchar_t;
typedef struct { int quot, rem; } div_t;
typedef struct { long int quot, rem; } ldiv_t;


double atof ( const char* );
int atoi ( const char* );
long int atol ( const char* );
double strtod ( const char*, char** );
long int strtol ( const char*, char**, int );
unsigned long int strtoul ( const char*, char**, int );

int rand ( void );
void srand ( unsigned int );

void* malloc ( size_t ) MALLOC;
void* calloc ( size_t, size_t ) MALLOC;
void* realloc ( void*, size_t );
void free ( void* );

void abort ( void ) NORETURN;
void exit ( int ) NORETURN;
int atexit ( void (*)( void ) );

int system ( const char* );
char* getenv ( const char* );

void* bsearch ( const void*, const void*, size_t, size_t,
	int (*)( const void*, const void* ) );
void qsort ( const void*, size_t, size_t,
	int (*)( const void*, const void* ) );

div_t div ( int, int );
ldiv_t ldiv ( long int, long int );

extern void __locale_mb_cur_max ( void );


#endif // LIBSTD_STDLIB_HEADER

