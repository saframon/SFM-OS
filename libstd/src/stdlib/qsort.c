
/* ----------------------------------------------------------------- *
 *
 *   qsort.c: provides a sorting algorithm for arbitary
 *            variable types.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the C standard library for the
 *   SFM-OS kernel, and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>


static void swap ( const void* a, const void* b, size_t s )
{
	// create char pointers for `a' and `b' and `end'
	char* e = (char*)( a ) + s;
	char* p = (char*)( a );
	char* q = (char*)( b );

	// copy over each byte seperately
	for ( ; p < e; ++p, ++q )
	{
		// swap those bytes
		char t = *p;
		*p = *q;
		*q = t;
	}
}


void qsort ( const void* b, size_t n, size_t s,
	int (*compare)( const void*, const void* ) )
{
	// we will implement bubble sort for the moment,
	// as writing a good iterative quick sort is not
	// intuitively done
	//
	// TODO: implement a proper quicksort algorithm

	// create char pointers for `b'
	const char* p = (const char*)( b );

	// loop through all elements in the array
	for ( size_t i = n; i > 0; --i )
	{
		// loop through all elements in the array,
		// starting at `p'
		for ( size_t j = 0; j + 1 < i; ++j )
		{
			// test if the element at `j' is smaller than
			// the one at `j+1'
			if ( __builtin_expect( compare(
				(const void*)( p + ( j * s ) ),
				(const void*)( p + ( ( j + 1 ) * s ) ) )
				> 0, FALSE ) )
			{
				// swap `p' and `q'
				swap( (const void*)( p + ( j * s ) ),
					(const void*)( p + ( ( j + 1 )
					* s ) ), s );
			}

		}
	}
}

