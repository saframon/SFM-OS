
/* ----------------------------------------------------------------- *
 *
 *   strncmp.c: lexicographical comparision of two strings, but
 *              with a maximum comparision length.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the C standard library for the
 *   SFM-OS kernel, and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <stddef.h>
#include <stdint.h>
#include <string.h>


#define WORDSIZE		sizeof(size_t)
#define UNALIGNED(x)		( (uintptr_t)( (x) ) & ( WORDSIZE - 1 ) )
#define UNALIGNED2(x,y)		( UNALIGNED( (x) ) || UNALIGNED( (y) ) )
#define DETECTNUL(x,h,l)	( ( (x) - (l) ) & (h) )


int strncmp ( const char* a, const char* b, size_t n )
{
	// variables for the algorithm
	char* p = (char*)( a );
	char* q = (char*)( b );
	size_t i = 0;

#ifdef __OPTIMIZE__

	// more variables
	size_t magic_hi, magic_lo;

	// initialize the magic values. Those if statements
	// should be optimized away by the compiler
	if ( WORDSIZE == 4 )
	{
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Woverflow"
		magic_hi = 0x80808080L;
		magic_lo = 0x01010101L;
#pragma GCC diagnostic pop
	}
	else if ( WORDSIZE == 8 )
	{
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Woverflow"
		magic_hi = 0x8080808080808080L;
		magic_lo = 0x0101010101010101L;
#pragma GCC diagnostic pop
	}
	else
	{
		return 0;
	}

	// loop the first few bytes until we are word
	// aligned
	while ( __builtin_expect( UNALIGNED2( p, q ), TRUE ) )
	{
		// check whether we reached the character
		// limit
		if ( __builtin_expect( i >= n, FALSE ) )
		{
			// return
			return 0;
		}

		// check whether we already reached the NUL
		// byte, or found two diverse characters.
		if ( __builtin_expect( *p == '\0' || *p != *q, FALSE ) )
		{
			// return the difference of `p' to `q'
			return *( (uint8_t*)( p ) ) - *( (uint8_t*)( q ) );
		}

		// increase `i'
		++i;

		// increase `p' and `q'
		++p;
		++q;
	}

	// create word size pointers for `p' and `q'
	size_t* _p = (size_t*)( p ), *_q = (size_t*)( q );

	// loop through the string, until we either detect
	// the NUL byte or find two unequal characters.
	// Also check whether we`re still inside the bounds
	// of the character limit
	while ( __builtin_expect( i + WORDSIZE <= n
		&& !DETECTNUL( *_p, magic_hi, magic_lo )
		&& *_p == *_q, TRUE ) )
	{
		// increase `i'
		i += WORDSIZE;

		// increase `_p' and `_q'
		++_p;
		++_q;
	}

	// update `p' and `q'
	p = (char*)( _p );
	q = (char*)( _q );
	
#endif // __OPTIMIZE__
	
	// loop through the remaining bytes until
	// we either hit the NUL byte, or find two
	// diverse characters. Also check whether we
	// reached the character limit
	while ( __builtin_expect( *p && *p == *q && i < n, TRUE ) )
	{
		// increase `i'
		++i;

		// increase `p' and `q'
		++p;
		++q;
	}

	// check whether we reached the character limit
	if ( __builtin_expect( i >= n, FALSE ) )
	{
		// return
		return 0;
	}

	// return the difference of `p' to `q'
	return *( (uint8_t*)( p ) ) - *( (uint8_t*)( q ) );
}

