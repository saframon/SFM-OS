
/* ----------------------------------------------------------------- *
 *
 *   strlen.c: get the length of a string.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the C standard library for the
 *   SFM-OS kernel, and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <stddef.h>
#include <stdint.h>
#include <string.h>


#define WORDSIZE		sizeof(size_t)
#define UNALIGNED(x)		( (uintptr_t)( (x) ) & ( WORDSIZE - 1 ) )
#define DETECTNUL(x,h,l)	( ( (x) - (l) ) & (h) )


size_t strlen ( const char* s )
{
	// variable for the algorithm
	char* p = (char*)( s );

#ifdef __OPTIMIZE__

	// more variables
	size_t magic_hi, magic_lo;

	// initialize the magic values. Those if statements
	// should be optimized away by the compiler
	if ( WORDSIZE == 4 )
	{
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Woverflow"
		magic_hi = 0x80808080L;
		magic_lo = 0x01010101L;
#pragma GCC diagnostic pop
	}
	else if ( WORDSIZE == 8 )
	{
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Woverflow"
		magic_hi = 0x8080808080808080L;
		magic_lo = 0x0101010101010101L;
#pragma GCC diagnostic pop
	}
	else
	{
		return 0;
	}

	// loop the first few bytes until we are word
	// aligned
	while ( __builtin_expect( UNALIGNED( p ), TRUE ) )
	{
		// check whether we already reached
		// a NUL byte
		if ( __builtin_expect( *p == '\0', FALSE ) )
		{
			// return the difference of `p' to
			// the original string pointer
			return p - s;
		}

		// increase `p'
		++p;
	}

	// set q to point to the word aligned
	// string pointer
	size_t* q = (size_t*)( p );

	// loop through the string, until we detect
	// the NUL byte
	while ( __builtin_expect(
		!DETECTNUL( *q, magic_hi, magic_lo ), TRUE ) )
	{
		// increase `q'
		++q;
	}

	// update `p' to point to `q'
	p = (char*)( q );
	
#endif // __OPTIMIZE__
	
	// loop through the remaining bytes until
	// we actually hit NUL
	while ( __builtin_expect( *p, TRUE ) )
	{
		// increase `p'
		++p;
	}

	// return the difference of `p' to the original
	// string pointer
	return p - s;
}

