# SFM-OS
[![pipeline status](https://gitlab.com/saframon/SFM-OS/badges/master/pipeline.svg)](https://gitlab.com/saframon/SFM-OS/commits/master)
[![coverage report](https://gitlab.com/saframon/SFM-OS/badges/master/coverage.svg)](https://gitlab.com/saframon/SFM-OS/commits/master)

A simple and basic operating system (including bootloader, kernel and userspace) for x86 architectures (and hopefully other in the future) written in Assembly and C.
Motivation for this project is to get a deep understanding on how bootloaders, kernels etc. work at their core. The code for this project has been greatly inspired by the Linux source code, as well as other GNU tools.

### Build Instructions:

> NOTE:
> SFM-OS should compile on any ***NIX**-system, though it has only been tested on MacOS and Ubuntu/Debian.
> Make sure that the following packages are installed: `bzip2`, `curl`, `dosfstools`, `gcovr`, `git`, `make`, `mtools` and `xorriso`.

First, the cross-compiler toolchain needs to be downloaded for the requested target architecture. In this case, we request the download of the pre-compiled toolchain for the `x86`-Architecture
```shell
$ make toolchain ARCH=x86
```

Next, compile a target for that architecture. Valid targets are `loader`, `kernel` or `libstd`. Note that the `kernel` target implicitly builds the `libstd` target
```shell
$ make loader ARCH=x86
$ make kernel ARCH=x86
```

Finally, if desired, the compiled binaries can be fully installed on the system:
```shell
$ make install-loader
$ make install-kernel
```

For more information on the available commands and targets, take a look to the help target:
```shell
$ make help
```

### Copyright notice:

```
MIT License

Copyright (c) 2017-2018 Sandro Francesco Montemezzani

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

