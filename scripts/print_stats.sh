#!/bin/bash


HORIZONTAL_LINE=$(printf '%0.1s' "-"{0..69})

TOTAL_LINES_OF_CODE_CODE=0
TOTAL_LINES_OF_CODE_EMPTY=0
TOTAL_LINES_OF_CODE_TOTAL=0


printf "\n%s\n" $HORIZONTAL_LINE

while [[ $# -gt 0 ]]
do
	filename="$1"
	shift

	total=$(( 0 + $(wc -l < $filename) ))
	code=$(( 0 + $(egrep -v -e '^\s*$' -e '^\s*\/\/' -e '^\s*#\s+' $filename | wc -l) ))
	empty=$(( total - code ))

	filename_base=${filename##*/}

	printf " %s%*s%*s total%*s code%*s empty\n" $filename_base $((32-${#filename_base})) '' 6 "$total" 6 "$code" 6 "$empty"

	TOTAL_LINES_OF_CODE_CODE=$(( TOTAL_LINES_OF_CODE_CODE + code ))
	TOTAL_LINES_OF_CODE_EMPTY=$(( TOTAL_LINES_OF_CODE_EMPTY + empty ))
	TOTAL_LINES_OF_CODE_TOTAL=$(( TOTAL_LINES_OF_CODE_TOTAL + total ))
done


printf "%s\n%*s" $HORIZONTAL_LINE 33 ''
printf "%*s total%*s code%*s empty\n\n" 6 "$TOTAL_LINES_OF_CODE_TOTAL" 6 "$TOTAL_LINES_OF_CODE_CODE" 6 "$TOTAL_LINES_OF_CODE_EMPTY"
