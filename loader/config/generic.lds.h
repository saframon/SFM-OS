
//
//
//   generic.lds.h: linker script receipt for a generic target.
//
//   Copyright (C) 2018 Sandro Francesco Montemezzani
//   Author: Sandro Montemezzani <sandro@montemezzani.net>
//
//   This file is part of the bootloader for the SFM-OS kernel,
//   and is licensed under the MIT License.
//
//


ENTRY(_start)


SECTIONS
{
    PROVIDE(__loader_start = .);

    .boot : AT(0x00)
    {
        PROVIDE(__boot_start = .);

        *(.text)
        *(.rodata*)
        *(.data)
        *(.bss)

        PROVIDE(__boot_end = .);
    }

    PROVIDE(__loader_end = .);

    /DISCARD/ : 
    {
        *(.comment)
    }
}

