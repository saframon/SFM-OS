
#
#
#   x86.mk: configuration for the x86 architecture.
#
#   Copyright (C) 2018 Sandro Francesco Montemezzani
#   Author: Sandro Montemezzani <sandro@montemezzani.net>
#
#   This file is part of the SFM-OS project, and is licensed
#   under the MIT License.
#
#


########################
#  UPDATE FILES SETUP  #
########################

LINKER_SCRIPT_RECEIPT = $(configdir)/i386.lds.h


#############################
#  UPDATE COMPILER OPTIONS  #
#############################

CROSS_CFLAGS   += -m32 -march=i386 -mno-sse -mno-mmx \
                  -fno-unwind-tables -fno-asynchronous-unwind-tables \
                  -fno-stack-protector -freg-struct-return
CROSS_CPPFLAGS += -D__i386__


#######################
#  COMPILATION UNITS  #
#######################

OBJECT_FILES += \
	$(builddir)/obj/arch/x86/a20.o \
	$(builddir)/obj/arch/x86/boot0.o \
	$(builddir)/obj/arch/x86/boot1.o \
	$(builddir)/obj/arch/x86/e801.o \
	$(builddir)/obj/arch/x86/e820.o \
	$(builddir)/obj/arch/x86/fat12_filename.o \
	$(builddir)/obj/arch/x86/fat12_read_file.o \
	$(builddir)/obj/arch/x86/int12h.o \
	$(builddir)/obj/arch/x86/protected_putc.o \
	$(builddir)/obj/arch/x86/protected_puts.o \
	$(builddir)/obj/arch/x86/read_sectors.o \
	$(builddir)/obj/arch/x86/real_putc.o \
	$(builddir)/obj/arch/x86/real_puts.o \
	$(builddir)/obj/arch/x86/string_length16.o \
	$(builddir)/obj/arch/x86/text.o

