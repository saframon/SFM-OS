
/* ----------------------------------------------------------------- *
 *
 *   text.inc: various definitions regarding the VGA
 *             text mode output
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the bootloader for the SFM-OS kernel,
 *   and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


.ifndef	LOADER_TEXT_INCLUDE
.set	LOADER_TEXT_INCLUDE, 1

	.set	text_memory_base, 0x000b8000
	.set	text_mode_columns, 80
	.set	text_mode_rows, 25

.endif // LOADER_TEXT_INCLUDE

