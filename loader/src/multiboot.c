
/* ----------------------------------------------------------------- *
 *
 *   multiboot.c: initialization routine for the multiboot
 *                structure when passing control to the kernel
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the bootloader for the SFM-OS kernel,
 *   and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <loader.h>
#include <multiboot.h>


#define MULTIBOOT_TAG_ALIGNMENT	8
#define TAG_ALIGN(x)	( (x) = (__typeof__(x))( ( (__UINTPTR_TYPE__)(x) \
	+ MULTIBOOT_TAG_ALIGNMENT - 1 ) & ( -MULTIBOOT_TAG_ALIGNMENT ) ) )


extern void* copy_memory ( void*, const void*, unsigned int ) BOOT1_FUNC;
extern void protected_putc ( char ) BOOT1_FUNC;
extern void protected_puts ( char[] ) BOOT1_FUNC;
extern unsigned int string_length ( char[] ) BOOT1_FUNC;


extern char __version_str1__[] BOOT1_DATA;

extern multiboot_uint32_t mem_lower BOOT1_DATA;
extern multiboot_uint32_t mem_upper BOOT1_DATA;
extern multiboot_uint32_t mmap_base BOOT1_DATA;
extern multiboot_uint32_t mmap_end BOOT1_DATA;


BOOT1_DATA char spec_string[] =
	"using Multiboot2 Specification version 2.0 from\r\n"
	"https://www.gnu.org/software/grub/manual/multiboot2/multiboot.html"
	"\r\n";

BOOT1_DATA char __newln[] = "\r\n";
BOOT1_DATA char done_message[] = "Done.\r\n";
BOOT1_DATA char search_header_message[] =
	"Scanning memory for the multiboot header structure... ";
BOOT1_DATA char parse_header_tags_message[] =
	"Parse the provided multiboot header tags... ";
BOOT1_DATA char read_inforequest_message[] =
	"Read the passed information request tag... ";
BOOT1_DATA char cmdline_tag_message[] =
	"Create the command line info tag... ";
BOOT1_DATA char bootloader_tag_message[] =
	"Create the bootloader info tag... ";
BOOT1_DATA char meminfo_tag_message[] =
	"Create the memory info tag... ";
BOOT1_DATA char mmap_tag_message[] =
	"Create the MMAP info tag... ";


#define MB_TAGREQ_CMDLINE		( 1 << 0 )
#define MB_TAGREQ_BOOTLOADER		( 1 << 1 )
#define MB_TAGREQ_MODULE		( 1 << 2 )
#define MB_TAGREQ_MEMORY_INFO		( 1 << 3 )
#define MB_TAGREQ_BOOT_DEVICE		( 1 << 4 )
#define MB_TAGREQ_MMAP			( 1 << 5 )
#define MB_TAGREQ_VBE			( 1 << 6 )
#define MB_TAGREQ_FRAMEBUFFER		( 1 << 7 )
#define MB_TAGREQ_ELF_SECTIONS		( 1 << 8 )
#define MB_TAGREQ_APM			( 1 << 9 )
#define MB_TAGREQ_EFI32			( 1 << 10 )
#define MB_TAGREQ_EFI64			( 1 << 11 )
#define MB_TAGREQ_SMBIOS		( 1 << 12 )
#define MB_TAGREQ_ACPI_OLD		( 1 << 13 )
#define MB_TAGREQ_ACPI_NEW		( 1 << 14 )
#define MB_TAGREQ_NETWORK		( 1 << 15 )
#define MB_TAGREQ_EFI_MMAP		( 1 << 16 )
#define MB_TAGREQ_EFI_NT		( 1 << 17 )
#define MB_TAGREQ_EFI32_IH		( 1 << 18 )
#define MB_TAGREQ_EFI64_IH		( 1 << 19 )
#define MB_TAGREQ_LOAD_BASE		( 1 << 20 )


struct multiboot_header_packet
{
	struct multiboot_header_tag_inforequest*	inforequest;
	struct multiboot_header_tag_address*		address;
	struct multiboot_header_tag_entry*		entry;
	struct multiboot_header_tag_flags*		flags;
	struct multiboot_header_tag_framebuffer*	framebuffer;
	struct multiboot_header_tag_entry*		efi32_entry;
	struct multiboot_header_tag_entry*		efi64_entry;
	struct multiboot_header_tag_relocatable*	relocatable;
	int module_align, efiboot;
};


BOOT1_FUNC int multiboot_handle (
	struct multiboot_info* const mbi,
	const char* const kernel,
	void** const load_addr,
	unsigned int* const load_amount,
	void** const entry_addr )
{
	// print the specification message to the screen
	protected_puts( __newln );
	protected_puts( spec_string );
	protected_puts( __newln );

	// initialize the main fields of the
	// multiboot info structure. The tags will be initialized
	// and appended step by step later on.
	mbi->total_size = sizeof(struct multiboot_info);
	mbi->reserved = 0x00000000;
	char* mbi_ptr = (char*)( mbi->tags );

	// print the search header message
	protected_puts( search_header_message );

	// setup a variable pointing to the multiboot header provided
	// by the kernel image. Idealy, this pointer is immediately
	// correct, but we might have to shift it a bit
	struct multiboot_header* mbhdr = (struct multiboot_header*)( kernel );

	// temporary store the current offset while searching
	// for the multiboot header structure
	unsigned int header_offset = 0;

search_loop:
	// loop until either the multiboot magic is found or
	// the offset exceeds the defined maximum
	while ( LIKELY( mbhdr->magic != MULTIBOOT_HEADER_MAGIC ) )
	{
		// DEBUG
		protected_putc( '.' );

		// check whether we are still in the bounds with
		// current search offset
		if ( UNLIKELY( header_offset
			>= MULTIBOOT_HEADER_SEARCH_OFFSET ) )
		{
			// we could't find the multiboot header
			// within the first bytes of the provided
			// kernel image. We will have to abort
			return 1;
		}

		// increase the offset and update the pointer to the
		// multiboot header structure
		header_offset++;
		mbhdr = (struct multiboot_header*)( kernel + header_offset );
	}

	// DEBUG
	protected_putc( ':' );

	// check if we actually found the multiboot header,
	// testing it's checksum
	if ( UNLIKELY( mbhdr->magic + mbhdr->architecture
		+ mbhdr->header_length + mbhdr->checksum != 0 ) )
	{
		// we found the multiboot header magic, but it
		// actually was a false alarm. Reenter the loop above
		header_offset++;
		mbhdr = (struct multiboot_header*)( kernel + header_offset );
		goto search_loop;
	}

	// print the done message
	protected_puts( done_message );

	// multiboot header packet to keep track on the
	// passed header tags
	struct multiboot_header_packet header_packet = 
		{ 0,0,0,0,0,0,0,0,0,0 };

	// a header tag iterator for the passed header tags
	struct multiboot_header_tag* header_tag_iterator = mbhdr->tags;

	// print the parse header tags message
	protected_puts( parse_header_tags_message );

	// as long as the next header tag is not the
	// end tag, we can process it's information and
	// put it into the fields above
	while ( LIKELY( header_tag_iterator->type
		!= MULTIBOOT_HEADER_TAG_END ) )
	{
		// DEBUG
		protected_putc( '.' );

		// switch through the different tag types in
		// order to handle the correct one
		switch ( header_tag_iterator->type )
		{
		case MULTIBOOT_HEADER_TAG_INFO_REQUEST:
			header_packet.inforequest =
				(struct multiboot_header_tag_inforequest*)
				( header_tag_iterator );
			break;
		case MULTIBOOT_HEADER_TAG_ADDRESS:
			header_packet.address =
				(struct multiboot_header_tag_address*)
				( header_tag_iterator );
			break;
		case MULTIBOOT_HEADER_TAG_ENTRY:
			header_packet.entry =
				(struct multiboot_header_tag_entry*)
				( header_tag_iterator );
			break;
		case MULTIBOOT_HEADER_TAG_CONSOLE_FLAGS:
			header_packet.flags =
				(struct multiboot_header_tag_flags*)
				( header_tag_iterator );
			break;
		case MULTIBOOT_HEADER_TAG_FRAMEBUFFER:
			header_packet.framebuffer =
				(struct multiboot_header_tag_framebuffer*)
				( header_tag_iterator );
			break;
		case MULTIBOOT_HEADER_TAG_MODULE_ALIGN:
			header_packet.module_align = 1;
			break;
		case MULTIBOOT_HEADER_TAG_EFIBOOT:
			header_packet.efiboot = 1;
			break;
		case MULTIBOOT_HEADER_TAG_ENTRY_EFI32:
			header_packet.efi32_entry =
				(struct multiboot_header_tag_entry*)
				( header_tag_iterator );
			break;
		case MULTIBOOT_HEADER_TAG_ENTRY_EFI64:
			header_packet.efi64_entry =
				(struct multiboot_header_tag_entry*)
				( header_tag_iterator );
			break;
		case MULTIBOOT_HEADER_TAG_RELOCATABLE:
			header_packet.relocatable =
				(struct multiboot_header_tag_relocatable*)
				( header_tag_iterator );
			break;
		}

		// increase the header tag iterator
		header_tag_iterator = (struct multiboot_header_tag*)
			( (char*)( header_tag_iterator )
				+ header_tag_iterator->size );
	}

	// print the done message
	protected_puts( done_message );

	// create a bitfield to keep track on which
	// tags are requested
	unsigned int requested_tags = 0;

	// print the read information request message
	protected_puts( read_inforequest_message );

	// check whether the info request tag was provided in
	// multiboot header structure
	if ( UNLIKELY( header_packet.inforequest == 0 ) )
	{
		// no information request tag has been provided.
		// We will have to abort at this point
		return 1;
	}

	// a simple 4 byte iterator for the 'tags' field in the
	// information request structure
	multiboot_uint32_t* request_tag_iterator =
		header_packet.inforequest->mbi_tag_types;

	// define the end of the request tag array
	multiboot_uint32_t* request_tag_end = (multiboot_uint32_t*)
		( (char*)( header_packet.inforequest )
			+ header_packet.inforequest->size );

	// iterate through the request tag array and add the
	// matching flags to the bitfield created above
	while ( LIKELY( request_tag_iterator < request_tag_end ) )
	{
		// DEBUG
		protected_putc( '.' );

		// switch through the available tag types
		switch ( *request_tag_iterator )
		{
		case MULTIBOOT_TAG_CMDLINE:
			requested_tags |= MB_TAGREQ_CMDLINE;
			break;
		case MULTIBOOT_TAG_BOOTLOADER:
			requested_tags |= MB_TAGREQ_BOOTLOADER;
			break;
		case MULTIBOOT_TAG_MODULE:
			requested_tags |= MB_TAGREQ_MODULE;
			break;
		case MULTIBOOT_TAG_MEMORY_INFO:
			requested_tags |= MB_TAGREQ_MEMORY_INFO;
			break;
		case MULTIBOOT_TAG_BOOT_DEVICE:
			requested_tags |= MB_TAGREQ_BOOT_DEVICE;
			break;
		case MULTIBOOT_TAG_MMAP:
			requested_tags |= MB_TAGREQ_MMAP;
			break;
		case MULTIBOOT_TAG_VBE:
			requested_tags |= MB_TAGREQ_VBE;
			break;
		case MULTIBOOT_TAG_FRAMEBUFFER:
			requested_tags |= MB_TAGREQ_FRAMEBUFFER;
			break;
		case MULTIBOOT_TAG_ELF_SECTIONS:
			requested_tags |= MB_TAGREQ_ELF_SECTIONS;
			break;
		case MULTIBOOT_TAG_APM:
			requested_tags |= MB_TAGREQ_APM;
			break;
		case MULTIBOOT_TAG_EFI32:
			requested_tags |= MB_TAGREQ_EFI32;
			break;
		case MULTIBOOT_TAG_EFI64:
			requested_tags |= MB_TAGREQ_EFI64;
			break;
		case MULTIBOOT_TAG_SMBIOS:
			requested_tags |= MB_TAGREQ_SMBIOS;
			break;
		case MULTIBOOT_TAG_ACPI_OLD:
			requested_tags |= MB_TAGREQ_ACPI_OLD;
			break;
		case MULTIBOOT_TAG_ACPI_NEW:
			requested_tags |= MB_TAGREQ_ACPI_NEW;
			break;
		case MULTIBOOT_TAG_NETWORK:
			requested_tags |= MB_TAGREQ_NETWORK;
			break;
		case MULTIBOOT_TAG_EFI_MMAP:
			requested_tags |= MB_TAGREQ_EFI_MMAP;
			break;
		case MULTIBOOT_TAG_EFI_NT:
			requested_tags |= MB_TAGREQ_EFI_NT;
			break;
		case MULTIBOOT_TAG_EFI32_IH:
			requested_tags |= MB_TAGREQ_EFI32_IH;
			break;
		case MULTIBOOT_TAG_EFI64_IH:
			requested_tags |= MB_TAGREQ_EFI64_IH;
			break;
		case MULTIBOOT_TAG_LOAD_BASE:
			requested_tags |= MB_TAGREQ_LOAD_BASE;
			break;
		}

		// increase the request tag iterator
		request_tag_iterator++;
	}

	// print the done message
	protected_puts( done_message );

	// check whether the basic address tag was provided in
	// multiboot header structure
	if ( UNLIKELY( header_packet.address == 0 ) )
	{
		// no basic address tag has been provided.
		// We will have to abort at this point
		return 1;
	}

	// write the load address to the specified location.
	// For the moment, we will not use the address specified
	// by the header tag, instead we will just subtract the search
	// offset from the multiboot header address.
	*load_addr = (void*)( (char*)( header_packet.address->header_addr )
		- header_offset );

	// write the amount of bytes to load to the specified
	// location
	*load_amount = (unsigned int)( header_packet.address->load_end_addr )
		- (unsigned int)( *load_addr );

	// Also clear the bss section for the kernel. It is
	// assumed, that the bss section goes from load_end
	// to the specified bss_end address.
	for ( char* p = (char*)( header_packet.address->load_end_addr );
		p < (char*)( header_packet.address->bss_end_addr ); ++p )
	{
		// set that byte to zero
		*p = 0x00;
		//protected_putc( '@' );
	}

	// check whether the entry address tag was provided in
	// multiboot header structure
	if ( LIKELY( header_packet.entry == 0 ) )
	{
		// no entry address tag has been provided.
		// We will have to abort at this point
		return 1;
	}

	// write the entry address to the specified location
	*entry_addr = (void*)( header_packet.entry->entry_addr );

	// if the command line info structure is requested,
	// build up the corresponding mbi tag
	if ( LIKELY( requested_tags & MB_TAGREQ_CMDLINE ) )
	{
		// print the cmdline info tag message
		protected_puts( cmdline_tag_message );

		// create a dedicated pointer for the
		// cmdline info tag
		struct multiboot_info_tag_command_line* p =
			(struct multiboot_info_tag_command_line*)( mbi_ptr );

		// variable
		char cmdline[] = "debug=text,serial";

		// initialize the structure
		p->type = MULTIBOOT_TAG_CMDLINE;
		p->size = sizeof(*p) + sizeof(cmdline);
		copy_memory( p->string, cmdline, sizeof(cmdline) );

		// increase and align `mbi_ptr'
		mbi_ptr += p->size;
		TAG_ALIGN( mbi_ptr );
	}

	// if the bootloader name info structure is requested,
	// build up the corresponding mbi tag
	if ( LIKELY( requested_tags & MB_TAGREQ_BOOTLOADER ) )
	{
		// print the bootloader info tag message
		protected_puts( bootloader_tag_message );

		// create a dedicated pointer for the
		// bootloader info tag
		struct multiboot_info_tag_bootloader_name* p =
			(struct multiboot_info_tag_bootloader_name*)(
			mbi_ptr );

		// initialize the structure
		p->type = MULTIBOOT_TAG_BOOTLOADER;
		p->size = sizeof(*p);

		// get the length of the version string of
		// the bootloader
		int n = string_length( __version_str1__ );

		// copy the bootloader name into the structure
		copy_memory( (void*)( p->string ),
			(void*)( __version_str1__ ), n + 1 );
		p->size += n + 1;

		// increase and align `mbi_ptr'
		mbi_ptr += p->size;
		TAG_ALIGN( mbi_ptr );
	}

	// if the basic memory info structure is requested, build
	// up the corresponding mbi tag
	if ( LIKELY( requested_tags & MB_TAGREQ_MEMORY_INFO ) )
	{
		// print the memory info tag message
		protected_puts( meminfo_tag_message );

		// create a dedicated pointer for the memory info tag
		struct multiboot_info_tag_memory* p =
			(struct multiboot_info_tag_memory*)( mbi_ptr );

		// initialize the structure
		p->type = MULTIBOOT_TAG_MEMORY_INFO;
		p->size = sizeof(*p);
		p->mem_lower = mem_lower;
		p->mem_upper = mem_upper;

		// increase and align `mbi_ptr'
		mbi_ptr += p->size;
		TAG_ALIGN( mbi_ptr );

		// print the done message
		protected_puts( done_message );
	}

	// if the MMAP structure is requested, build up the
	// corresponding mbi tag
	if ( LIKELY( requested_tags & MB_TAGREQ_MMAP ) )
	{
		// print the mmap tag message
		protected_puts( mmap_tag_message );

		// create a dedicated pointer for the mmap info tag
		struct multiboot_info_tag_mmap* p =
			(struct multiboot_info_tag_mmap*)( mbi_ptr );

		// initialize the structure
		p->type = MULTIBOOT_TAG_MMAP;
		p->size = sizeof(*p);
		p->entry_size = sizeof(struct multiboot_mmap_entry);
		p->entry_version = 0x00;

		// increase `mbi_ptr'
		mbi_ptr += p->size;

		// loop through the mmap count
		for ( struct multiboot_mmap_entry* e =
			(struct multiboot_mmap_entry*)( mmap_base );
			(unsigned int)( e ) < mmap_end; ++e )
		{
			// create a dedicated pointer for the mmap info tag
			struct multiboot_mmap_entry* q =
				(struct multiboot_mmap_entry*)( mbi_ptr );

			// copy the mmap entry into the info tag
			*q = *e;

			// normalize the reserved tag
			q->reserved = 0x00000000;

			// increase `mbi_ptr' and `size'
			mbi_ptr += sizeof(struct multiboot_mmap_entry);
			p->size += sizeof(struct multiboot_mmap_entry);
		}

		// align `mbi_ptr'
		TAG_ALIGN( mbi_ptr );

		// print the done message
		protected_puts( done_message );
	}

	// finally, construct the end info tag
	{
		// create a dedicated pointer for the mmap info tag
		struct multiboot_info_tag* p =
			(struct multiboot_info_tag*)( mbi_ptr );

		// initialize the structure
		p->type = MULTIBOOT_TAG_END;
		p->size = sizeof(*p);

		// increase and align `mbi_ptr'
		mbi_ptr += p->size;
		TAG_ALIGN( mbi_ptr );
	}

	// update `total_size' of the multiboot info
	mbi->total_size = mbi_ptr - (char*)( mbi );

	// we're done, return
	return 0;
}

