
/* ----------------------------------------------------------------- *
 *
 *   e801.s: get the extended memory bounds using the BIOS
 *           interrupt 0x15 AX=0xe801.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the bootloader for the SFM-OS kernel,
 *   and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


.code16
.org 0x00

.section .boot1, "awx", @progbits

.global e801
.type e801, @function

e801:
	// --------------
	// |   E801()   |
	// --------------

	// push the old base pointer to the stack
	push	%bp

	// set the new base pointer to the current stack pointer
	mov	%sp, %bp

	// store the current value of %bx and %cx on the stack
	push	%bx
	push	%cx

	// prepare all registers for the int call
	mov	$0xe801, %ax
	xor	%cx, %cx
	xor	%dx, %dx

	// do the int call
	int	$0x15

	// if the carry flag is set, return with an error
	jc	e801.error

	// %ah equals 0x86 means unsupported function
	cmp	$0x86, %ah
	je	e801.error

	// %ah equals 0x80 means invalid command
	cmp	$0x80, %ah
	je	e801.error

	// if %ax is zero, the results will most likely
	// be in %cx and %dx
	test	%ax, %ax
	jnz	e801.next

	// move the results into %ax and %bx
	mov	%cx, %ax
	mov	%dx, %bx

e801.next:
	// make sure that %ax = %eax and %bx = %ebx
	and	$0xFFFF, %eax
	and	$0xFFFF, %ebx

	// multiply %ebx by 64, and add the result
	// to %eax
	shl	$6, %ebx
	add	%ebx, %eax

	// store the result
	movl	%eax, mem_upper

	// restore the old values of %bx and %cx from the stack
	pop	%cx
	pop	%bx

	// clear %ax to signalize we terminated properly
	xor	%ax, %ax

	// restore the old base pointer from stack, and return
	pop	%bp
	ret

e801.error:
	// clear the carry bit
	clc

	// restore the old values of %bx and %cx from the stack
	pop	%cx
	pop	%bx

	// let %ax contain the error code 0x01
	mov	$0x01, %ax

	// restore the old base pointer from stack, and return
	pop	%bp
	ret


.code32
.section .boot1.data, "aw", @progbits

.balign 4

.global mem_upper
.type mem_upper, @object

mem_upper:
	// ----------------
	// |   MEM UPPER   |
	// ----------------

	.long	0x00000000

