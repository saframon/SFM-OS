
/* ----------------------------------------------------------------- *
 *
 *   fat12_read_file.s: search the file in the FAT12 filesystem
 *                      and read it into the specified buffer
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the bootloader for the SFM-OS kernel,
 *   and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


.extern fat12_filename
.extern read_sector
.extern real_putc
.extern real_puts
.extern string_length16

.code16
.org 0x00

.section .boot1, "awx", @progbits

.global fat12_read_file
.type fat12_read_file, @function

fat12_read_file:
	// ------------------------------------------------------
	// |   FAT12_READ_FILE(drive,filename,segment,offset)   |
	// ------------------------------------------------------

	// push the old base pointer to the stack
	push	%bp

	// set the new base pointer to the current stack pointer
	mov	%sp, %bp

	// save the original value of %bx, %cx, %di and %si
	push	%bx
	push	%cx
	push	%di
	push	%si

	// create space for the bootsector to be loaded
	sub	$512, %sp
	mov	%sp, %di

	// read the boot sector into the created space on the stack
	push	%di		/* param: offset */
	push	%ss		/* param: segment */
	push	$1		/* param: count */
	push	$0		/* param: sector */
	push	4(%bp)		/* param: drive */
	call	read_sectors
	add	$10, %sp

	// create enough space on the stack for the first FAT entry
	mov	22(%di), %bx	/* BPB: sectors per FAT */
	mov	%bx, %ax
	mov	$512, %cx
	mul	%cx
	sub	%ax, %sp
	mov	%sp, %si
	push	%ax

	// read the FAT sectors into the created space on stack
	push	%si		/* param: offset */
	push	%ss		/* param: segment */
	push	%bx		/* param: count */
	push	14(%di)		/* param: sector */
	push	4(%bp)		/* param: drive */
	call	read_sectors
	add	$10, %sp

	// calculate the size of the root directory, put the result into %cx
	mov	$32, %ax	/* 32 bytes per root dir entry */
	mov	17(%di), %bx	/* BPB: max root directory entries */
	mul	%bx
	mov	%ax, %cx

	// create space for the root directory entries, and then
	// save the size to the stack as well
	sub	%cx, %sp
	push	%cx
	mov	%sp, %cx
	add	$2, %cx
	push	%cx		/* param: offset */
	push	%ss		/* param: segment */

	// push the size in sectors to the stack
	xor	%dx, %dx
	mov	11(%di), %bx	/* BPB: bytes per sector */
	div	%bx
	push	%ax		/* param: count */

	// calculate the starting sector for the root directory entries
	// and store it to %ax
	xor	%ax, %ax
	movb	16(%di), %al	/* BPB: number of FATs on disk */
	mov	22(%di), %bx	/* BPB: sectors per FAT */
	mul	%bx
	add	14(%di), %ax	/* BPB: reserved sectors count */

	// read the root directory entries into the memory
	push	%ax		/* param: sector */
	push	4(%bp)		/* param: drive */
	call	read_sectors
	add	$10, %sp

	// get the directory entries buffer location
	// and save it into %cx
	mov	%sp, %cx
	add	$2, %cx

	// let %ax point to the first byte after the root entries
	mov	$32, %ax	/* 32 bytes per directory entry */
	mov	17(%di), %bx	/* BPB: max root directory entries */
	mul	%bx
	add	%cx, %ax

	// now call the provided utility function to recursively
	// step down the directory path
	push	10(%bp)		/* param: offset */
	push	8(%bp)		/* param: segment */
	push	6(%bp)		/* param: filename */
	push	%ax		/* param: end */
	push	%cx		/* param: entries */
	push	%si		/* param: fat */
	push	%di		/* param: bpb */
	call	__fat12_read_file
	add	$14, %sp

	// restore the size of the root directory entries, and pop
	// them from the stack
	pop	%cx
	add	%cx, %sp

	// restore the size of the FAT entry, and then clear it
	// them from the stack
	pop	%cx
	add	%cx, %sp

	// pop drive boot sector from stack
	add	$512, %sp

	// restore the original value of %bx, %cx, %di and %si
	pop	%si
	pop	%di
	pop	%cx
	pop	%bx

	// restore the old base pointer from stack, and return
	pop	%bp
	ret



__fat12_read_file:
	// ----------------------------------------------------------------------
	// |   __FAT12_READ_FILE(bpb,fat,entries,end,filename,segment,offset)   |
	// ----------------------------------------------------------------------

	// push the old base pointer to the stack
	push	%bp

	// set the new base pointer to the current stack pointer
	mov	%sp, %bp

	// save the original value of %bx, %cx, %si and %di
	push	%bx
	push	%cx
	push	%si
	push	%di

	// set %si to point to the provided filename buffer
	mov	12(%bp), %si

	// get the length of the provided filename
	push	%si		/* param: string */
	call	string_length16
	add	$2, %sp

	// make space on the stack for a copy of the filename
	dec	%sp
	mov	%sp, %di
	movb	$0x00, (%di)
	sub	%ax, %sp

	// set %di as the iterator for the filename copy
	mov	%sp, %di

	// we need to also store the length on the stack so we
	// can restore it later
	push	%ax

	// save the initial value of %di before we start looping
	// over the filename
	push	%di

__fat12_read_file.copyname.loop:
	// check if %ax reached 0x00
	and	%ax, %ax
	jz	__fat12_read_file.copyname.end

	// load the next character into %bx
	movb	(%si), %bl

	// check if we have the next character is a '/'
	cmpb	$'/', %bl
	je	__fat12_read_file.copyname.end

	// else, copy that character to the copy buffer
	movb	%bl, (%di)

	// increase both %si and %di, decrease %ax and
	// move on with the next iteration
	inc	%si
	inc	%di
	dec	%ax
	jmp	__fat12_read_file.copyname.loop

__fat12_read_file.copyname.end:
	// ensure the next character is the NUL character,
	// so we have a correct string, and then restore the
	// initial value of %di
	movb	$0x00, (%di)
	pop	%di

	// the stored filename pointer inside the base pointer should
	// be updated with the new value of %si
	mov	%si, 12(%bp)

	// create space for a 12 characters on the stack, and let %si
	// point to that location. This buffer will hold the DOS version
	// of the filename
	sub	$12, %sp
	mov	%sp, %si

	// the 12th byte should be NUL, to terminate the string
	movb	$0x00, 11(%si)

	// convert the filename to a DOS filename
	push	%si		/* param: buffer */
	push	%di		/* param: filename */
	call	fat12_filename
	add	$4, %sp

	// set %di to point to the first directory entry, and
	// %ax to point to the first byte after the last entry
	mov	8(%bp), %di
	mov	10(%bp), %ax

	// clear the direction flag, just to be sure
	cld

__fat12_read_file.direntries.loop:
	// if we have passed the last directory entry, exit the loop
	cmp	%di, %ax
	jbe	__fat12_read_file.direntries.nomatch

	// print a colon, for debug purposes
	push	%ax
	push	$':'
	call	real_putc
	add	$2, %sp
	pop	%ax

	// save %si and %di to prevend side effects of the next instruction
	push	%si
	push	%di

	// compare 11 characters in total
	mov	$11, %cx
	repe	cmpsb

	// restore %si and %di
	pop	%di
	pop	%si

	// if we have a match, exit the loop
	je	__fat12_read_file.match

	// increase %di by 32, so it points to the next entry,
	// and then move on with the next loop iteration
	add	$32, %di
	jmp	__fat12_read_file.direntries.loop

__fat12_read_file.direntries.nomatch:
	// pop the DOS filename string from stack
	add	$12, %sp

	// pop the copy of the filename from the stack
	pop	%ax
	add	%ax, %sp
	inc	%sp

	// restore the original value of %bx, %cx, %si and %di
	pop	%di
	pop	%si
	pop	%cx
	pop	%bx

	// clear the return value to 0x00
	mov	$0x0001, %ax

	// restore the old base pointer from stack, and return
	pop	%bp
	ret

__fat12_read_file.match:
	// load the attributes byte into %ax
	movb	11(%di), %al		/* Entry: attributes */

	// if either one of bits 0, 1, or 2 is set,
	// the entry represents a file
	testb	$0b00100111, %al
	jnz	__fat12_read_file.match.file

	// if not, reset %ax to hold the full attributes byte
	movb	11(%di), %al		/* Entry: attributes */

	// if bit 4 is set, the entry represents a subdirectory
	testb	$0b00010000, %al
	jnz	__fat12_read_file.match.subdir

	// else, abort as the other bits are not supported
	jmp	__fat12_read_file.fail

__fat12_read_file.match.file:
	// load the pointer to the resting filename into %ax
	mov	12(%bp), %bx

	// check wether it is the NUL character. If not, we have
	// a corrupt file entry, and should just abort
	cmpb	$0x00, (%bx)
	jne	__fat12_read_file.fail

	// store how many bytes per cluster we need in %ax, how
	// many sectors per cluster in %bx
	mov	4(%bp), %si
	mov	11(%si), %ax		/* BPB: bytes per sector */
	xor	%cx, %cx
	movb	13(%si), %cl		/* BPB: sectors per cluster */
	mul	%cx

	// create a local variable block on the stack, and let
	// %bx serve as a pointer to this block
	sub	$0x08, %sp
	mov	%sp, %bx
	mov	%ax, 0(%bx)		/* bytes per cluster */
	mov	%cx, 2(%bx)		/* sectors per cluster */
	mov	16(%bp), %ax
	mov	%ax, 4(%bx)		/* buffer pointer */
	mov	26(%di), %ax
	mov	%ax, 6(%bx)		/* next cluster number */

__fat12_read_file.match.file.loop:
	// check whether the next cluster number marks the
	// END-OF-CHAIN cluster
	mov	6(%bx), %ax
	cmp	$0xff8, %ax
	jae	__fat12_read_file.match.file.loop.end

	// store %bx on the stack, so it won't get lost in
	// one of the next operations
	push	%bx

	// calculate the actual sector number of the cluster,
	// and store it in %bx
	mov	6(%bx), %ax		/* Entry: first cluster */
	sub	$0x02, %ax
	mov	2(%bx), %bx
	mul	%bx
	mov	14(%si), %cx		/* BPB: reserved sectors count */
	add	%ax, %cx
	mov	22(%si), %ax		/* BPB: sectors per FAT */
	xor	%dx, %dx
	movb	16(%si), %dl		/* BPB: number of FATs on disk */
	mul	%dx
	add	%ax, %cx
	mov	17(%si), %ax		/* BPB: max root directory entries */
	mov	$32, %dx
	mul	%dx
	mov	11(%si), %bx		/* BPB: bytes per sector */
	xor	%dx, %dx
	div	%bx
	add	%ax, %cx

	// restore %bx, the pointer to the variable block
	pop	%bx

	// read the the cluster sectors into memory
	push	4(%bx)		/* param: offset */
	push	14(%bp)		/* param: segment */
	push	2(%bx)		/* param: count */
	push	%cx		/* param: sector */
	xor	%ax, %ax
	movb	36(%si), %al
	push	%ax		/* param: drive */
	call	read_sectors
	add	$10, %sp

	// increase the buffer pointer in the variable block
	mov	0(%bx), %ax
	add	%ax, 4(%bx)

	// store %bx on the stack, so it won't get lost in
	// one of the next operations
	push	%bx

	// load the current cluster from the FAT into %ax
	mov	6(%bx), %bx
	mov	%bx, %ax
	shr	$0x01, %ax
	add	%ax, %bx
	add	6(%bp), %bx
	mov	(%bx), %ax

	// restore %bx, the pointer to the variable block
	pop	%bx

	// check whether the current cluster number is odd or even
	mov	6(%bx), %cx
	test	$0x0001, %cx
	jz	__fat12_read_file.match.file.loop.cluster.even

__fat12_read_file.match.file.loop.cluster.odd:
	// shift the cluster value down by 4 bits
	shr     $0x04, %ax
	jmp	__fat12_read_file.match.file.loop.cluster.end

__fat12_read_file.match.file.loop.cluster.even:
	// mask out the most significant 4 bits of the
	// cluster value
	and	$0x0fff, %ax

__fat12_read_file.match.file.loop.cluster.end:
	// write the new cluster value into the variable block
	mov	%ax, 6(%bx)

	// move on with the next loop iteration
	jmp	__fat12_read_file.match.file.loop

__fat12_read_file.match.file.loop.end:
	// clear the variable block from the stack
	add	$0x08, %sp

	// wipe the return value %ax to zero
	xor	%ax, %ax

	// jump to the end of this procedure
	jmp	__fat12_read_file.end

__fat12_read_file.match.subdir:
	// load the pointer to the resting filename into %ax
	mov	12(%bp), %bx

	// check wether it is a '/'. If not, we have a corrupt
	// directory entry, and should just abort
	cmpb	$'/', (%bx)
	jne	__fat12_read_file.fail

	// increase the pointer to remove the '/' at the front
	inc	%bx
	mov	%bx, 12(%bp)

	// store how many bytes per cluster we need in %ax, how
	// many sectors per cluster in %bx
	mov	4(%bp), %si
	mov	11(%si), %ax		/* BPB: bytes per sector */
	xor	%cx, %cx
	movb	13(%si), %cl		/* BPB: sectors per cluster */
	mul	%cx

	// create a local variable block on the stack, and let
	// %bx serve as a pointer to this block
	sub	$0x08, %sp
	mov	%sp, %bx
	mov	%ax, 0(%bx)		/* bytes per cluster */
	mov	%cx, 2(%bx)		/* sectors per cluster */
	mov	%sp, 4(%bx)		/* entries end */
	mov	26(%di), %ax
	mov	%ax, 6(%bx)		/* next cluster number */

__fat12_read_file.match.subdir.loop:
	// check whether the next cluster number marks the
	// END-OF-CHAIN cluster
	mov	6(%bx), %ax
	cmp	$0xff8, %ax
	jae	__fat12_read_file.match.subdir.loop.end

	// make space for another cluster on the stack
	sub	0(%bx), %sp

	// store %bx on the stack, so it won't get lost in
	// one of the next operations
	push	%bx

	// calculate the actual sector number of the cluster,
	// and store it in %bx
	mov	6(%bx), %ax		/* Entry: first cluster */
	sub	$0x02, %ax
	mov	2(%bx), %bx
	mul	%bx
	mov	14(%si), %cx		/* BPB: reserved sectors count */
	add	%ax, %cx
	mov	22(%si), %ax		/* BPB: sectors per FAT */
	xor	%dx, %dx
	movb	16(%si), %dl		/* BPB: number of FATs on disk */
	mul	%dx
	add	%ax, %cx
	mov	17(%si), %ax		/* BPB: max root directory entries */
	mov	$32, %dx
	mul	%dx
	mov	11(%si), %bx		/* BPB: bytes per sector */
	xor	%dx, %dx
	div	%bx
	add	%ax, %cx

	// restore %bx, the pointer to the variable block
	pop	%bx

	// read the the cluster sectors into memory
	mov	%sp, %ax
	push	%ax		/* param: offset */
	push	%ss		/* param: segment */
	push	2(%bx)		/* param: count */
	push	%cx		/* param: sector */
	xor	%ax, %ax
	movb	36(%si), %al
	push	%ax		/* param: drive */
	call	read_sectors
	add	$10, %sp

	// store %bx on the stack, so it won't get lost in
	// one of the next operations
	push	%bx

	// load the current cluster from the FAT into %ax
	mov	6(%bx), %bx
	mov	%bx, %ax
	shr	$0x01, %ax
	add	%ax, %bx
	add	6(%bp), %bx
	mov	(%bx), %ax

	// restore %bx, the pointer to the variable block
	pop	%bx

	// check whether the current cluster number is odd or even
	mov	6(%bx), %cx
	test	$0x0001, %cx
	jz	__fat12_read_file.match.subdir.loop.cluster.even

__fat12_read_file.match.subdir.loop.cluster.odd:
	// shift the cluster value down by 4 bits
	shr     $0x04, %ax
	jmp	__fat12_read_file.match.subdir.loop.cluster.end

__fat12_read_file.match.subdir.loop.cluster.even:
	// mask out the most significant 4 bits of the
	// cluster value
	and	$0x0fff, %ax

__fat12_read_file.match.subdir.loop.cluster.end:
	// write the new cluster value into the variable block
	mov	%ax, 6(%bx)

	// move on with the next loop iteration
	jmp	__fat12_read_file.match.subdir.loop

__fat12_read_file.match.subdir.loop.end:
	// recursively call the current procedure, to evaluation
	// this directory
	mov	%sp, %ax
	push	16(%bp)		/* param: offset */
	push	14(%bp)		/* param: segment */
	push	12(%bp)		/* param: filename */
	push	4(%bx)		/* param: end */
	push	%ax		/* param: entries */
	push	6(%bp)		/* param: fat */
	push	4(%bp)		/* param: bpb */
	call	__fat12_read_file
	add	$14, %sp

	// clear the directory listings from the stack
	mov	4(%bx), %sp

	// clear the variable block from the stack
	add	$0x08, %sp

	// jump to the end of this procedure
	jmp	__fat12_read_file.end

__fat12_read_file.fail:
	// set the return value to 0x01
	mov	$0x0001, %ax

__fat12_read_file.end:
	// pop the DOS filename string from stack
	add	$12, %sp

	// pop the copy of the filename from the stack
	pop	%bx
	add	%bx, %sp
	inc	%sp

	// restore the original value of %bx, %cx, %si and %di
	pop	%di
	pop	%si
	pop	%cx
	pop	%bx

	// restore the old base pointer from stack, and return
	pop	%bp
	ret

