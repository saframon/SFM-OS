
/* ----------------------------------------------------------------- *
 *
 *   int12h.s: get the lower memory bounds using the BIOS
 *             interrupt 0x12.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the bootloader for the SFM-OS kernel,
 *   and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


.code16
.org 0x00

.section .boot1, "awx", @progbits

.global int12h
.type int12h, @function

int12h:
	// ----------------
	// |   INT12H()   |
	// ----------------

	// push the old base pointer to the stack
	push	%bp

	// set the new base pointer to the current stack pointer
	mov	%sp, %bp

	// make sure the carry flag is cleared
	clc

	// do the int call
	int	$0x12

	// if the carry flag is set, return with an error
	jc	int12h.error

	// store the return value
	movw	%ax, mem_lower

	// clear %ax to signalize we terminated properly
	xor	%ax, %ax

	// restore the old base pointer from stack, and return
	pop	%bp
	ret

int12h.error:
	// clear the carry bit
	clc

	// let %ax contain the error code 0x01
	mov	$0x01, %ax

	// restore the old base pointer from stack, and return
	pop	%bp
	ret


.code32
.section .boot1.data, "aw", @progbits

.balign 4

.global mem_lower
.type mem_lower, @object

mem_lower:
	// -----------------
	// |   MEM LOWER   |
	// -----------------

	.long	0x00000000

