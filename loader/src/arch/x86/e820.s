
/* ----------------------------------------------------------------- *
 *
 *   e820.s: load the MMAP structure using the BIOS
 *           interrupt 0x15 AX=0xe820.
 *
 *   Copyright (C) 2018 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the bootloader for the SFM-OS kernel,
 *   and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


	.set	mmap_memory_base, 0x0000ac00

.extern real_putc


.code16
.org 0x00

.section .boot1, "awx", @progbits

.global e820
.type e820, @function

e820:
	// --------------
	// |   E820()   |
	// --------------

	// push the old base pointer to the stack
	push	%bp

	// set the new base pointer to the current stack pointer
	mov	%sp, %bp

	// store the current values of %bx, %cx, %di and %es on the stack
	push	%bx
	push	%cx
	push	%di
	push	%es

	// make sure `mmap_end' is equal to `mmap_base'
	movl	mmap_base, %eax
	movl	%eax, mmap_end

	// let %es:%di point to 0x0e00:0x0000
	mov	%eax, %edx
	shr	$4, %edx
	push	%dx
	pop	%es
	mov	%eax, %edx
	and	$0x000F, %dx
	mov	%dx, %di

	// clear %ebx
	xor	%ebx, %ebx

e820.loop:
	// print a dot for debug purposes
	push	$'.'
	call	real_putc
	add	$2, %sp

	// force the next entry to be valid for ACPI3
	movl	$0x00000001, %es:20(%di)

	// prepare all registers for the int call
	mov	$0xe820, %eax
	mov	$24, %ecx
	mov	$0x534d4150, %edx

	// do the int call
	int	$0x15

	// if the carry flag is set, return with an error
	jc	e820.end

	// if %ebx is zero, that was the last entry
	test	%ebx, %ebx
	jz	e820.end

	// restore the magic value of %edx
	mov	$0x534d4150, %edx
	cmp	%eax, %edx
	jne	e820.error

	// if the loaded entry was only 20 bytes long,
	// we can surpass the next step
	cmp	$20, %cl
	jbe	e820.next.1

	// if the last bit of the reserved field is cleared
	// we should ignore this entry
	testb	$0x01, %es:20(%di)
	jz	e820.next.2

e820.next.1:
	// test if the length is zero
	movl	%es:8(%di), %ecx
	orl	%es:12(%di), %ecx
	jz	e820.next.2

	// increase %di by 24
	add	$24, %di

	// increase `mmap.end' as well
	addl	$24, mmap_end

e820.next.2:
	// jump to the next loop iteration
	jmp	e820.loop

e820.end:
	// clear the carry bit
	clc

	// restore the old values of %bx, %cx, %di and %es from the stack
	pop	%es
	pop	%di
	pop	%cx
	pop	%bx

	// clear %ax to signalize we terminated properly
	xor	%ax, %ax

	// restore the old base pointer from stack, and return
	pop	%bp
	ret

e820.error:
	// clear the carry bit
	clc

	// restore the old values of %bx, %cx, %di and %es from the stack
	pop	%es
	pop	%di
	pop	%cx
	pop	%bx

	// let %ax contain the error code 0x01
	mov	$0x01, %ax

	// restore the old base pointer from stack, and return
	pop	%bp
	ret


.code32
.section .boot1.data, "aw", @progbits

.balign 4

.global mmap_base
.type mmap_base, @object

mmap_base:
	// -----------------
	// |   MMAP BASE   |
	// -----------------

	.long	mmap_memory_base


.global mmap_end
.type mmap_end, @object

mmap_end:
	// ----------------
	// |   MMAP END   |
	// ----------------

	.long	mmap_memory_base

