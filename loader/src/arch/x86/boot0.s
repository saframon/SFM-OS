
/* ----------------------------------------------------------------- *
 *
 *   boot0.s: boot sector for floppy disks
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the bootloader for the SFM-OS kernel,
 *   and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


// ---------------------------------------------------------------------
// |   MAP OF BOTTOM HALF OF CONVENTIONAL MEMORY WHEN ENTERING BOOT0   |
// ---------------------------------------------------------------------

	// 00000..003FF: interrupt vector table
	// 00400..004FF: BIOS data area
	// 00500..07BFF: free memory
	// 07C00..07DFF: reserved for boot sector
	// 07E00..9FFFF: free memory
	// A0000..AFFFF: graphics video memory
	// B0000..B7FFF: monochrome text memory
	// B8000..BFFFF: colored text memory
	// C0000..FFFEF: ROM code area


.extern boot1

.code16
.org 0x00

.section .boot0, "awx", @progbits

.global _start
.type _start, @function

_start:
	// ------------------------------------
	// |   _START - BOOT0 - BOOT SECTOR   |
	// ------------------------------------

.global boot0.bpb
.type boot0.bpb, @object

boot0.bpb:
	// -------------------------------------------------
	// |   MASTER BOOT RECORD - BIOS PARAMETER BLOCK   |
	// -------------------------------------------------

	// this is the instruction where we jump to when the bootloader
	// gets loaded by the bios. So we'll just jump to the location
	// where the actual boot code begins
	jmp	boot0.start	/* 00..02: jump to the bootstrap code */
	nop

	// the actual BPB starts at offset 0x0B, so we have 8 bytes
	// left here that are unused. Let's just put our name in that
	.ascii	"SFMOS\0\0\0"	/* 03..0A: OEM label */

	// here is where the actual BPB begins. There are many sources
	// on the the internet where one can look up the BPB layout.
	// Main reference for this project was
	// http://thestarman.pcministry.com/asm/mbr/GRUBbpb.htm
	.word	512		/* 0B..0C: bytes per sector */
	.byte	1		/*     0D: sectors per cluster */
	.word	2		/* 0E..0F: reserved sectors count */
	.byte	2		/*     10: number of FATs on disk */
	.word	224		/* 11..12: max root directory entries */
	.word	2880		/* 13..14: total sectors on disk */
	.byte	0xf8		/*     15: media descriptor */
	.word	9		/* 16..17: sectors per FAT */
	.word	18		/* 18..19: sectors per track */
	.word	2		/* 1A..1B: number of heads */
	.fill	8, 1, 0x00	/* 1C..23: reserved for hard disks */

	// now comes the EBPB (Extended BIOS Parameter Block). This
	// one is specific to FAT12 systems.
	.skip	1		/*     24: boot drive number */
	.fill	1, 1, 0x00	/*     25: reserved (Windows NT) */
	.byte	0x29		/*     26: BPB signature byte */
	.long	0x00000000	/* 27..2A: volume serial number */
	.ascii	"SFMOSFLOPPY"	/* 2B..35: volume label */
	.ascii	"FAT12   "	/* 36..3B: file system ID */



boot0.start:
	// ------------------------------
	// |   BOOT0 - BOOTSTRAP CODE   |
	// ------------------------------

	// disable interrupts for the moment, we are unsave
	// as we will be changing segments for example
	cli		/* clear the interrupt flag */
	cld		/* clear the movement direction flag */

	// canonicalize codesegment and offset by doing a far jump
	// into 0000:next to make sure we're in codesegment 0.
	// We need this as sometimes, BIOSes load the bootloader
	// with 0x07C0:0x0000 instead of 0x0000:0x7C00
	ljmp	$0x00, $boot0.next

boot0.next:
	// store the our boot drive number so we can use it later
	movb	%dl, (boot0.bpb+0x24)	/* store the boot drive number */

	// initialize all segment registers
	xor	%ax, %ax	/* clear the %ax register to be zero */
	mov	%ax, %ds	/* assign the new data segment */
	mov	%ax, %es	/* assign the new extra segment */
	mov	%ax, %fs	/* assign the new general segment %fs */
	mov	%ax, %gs	/* assign the new general segment %gs */
	mov	%ax, %ss	/* assign the new stack segment */

	// set the boot0 origin as our stack top barrier for the boot1 code.
	// That means, with boot0 located at 0x7c00, we'll have around 30kb
	// of stack (in theory, the lowest part of the memory map is still
	// occupied, as you can see above)
	mov	$__boot0, %ax	/* take the address of boot0 code */
	mov	%ax, %bp	/* assign the new base pointer */
	mov	%ax, %sp	/* assign the new stack pointer */

	// reenable interrupts, as we are save again
	sti		/* set interrupt enable flag */
	std		/* set movement direction flag */

	// next, we load the next 22 sectors into 0x0000:0x8000 starting
	// from the third sector. The amount of remaining tries will be
	// stored in %di.
	mov	$0x04, %di	/* allow 4 tries in total */

boot0.load:
	// reset the drive we want to load sectors from
	movb	$0x00, %ah		/* define our bios function */
	movb	(boot0.bpb+0x24), %dl	/* load the stored drive number */
	int	$0x13			/* call the bios routine */

	// prepare all registers for the bios call
	movb	$0x02, %ah		/* define our bios function */
	movb	(boot0.bpb+0x0e), %al	/* load the reserved sectors count */
	subb	$2, %al
	movb	(boot0.bpb+0x24), %dl	/* load the stored drive number */
	movb	$0x00, %dh		/* load from the first head */
	movb	$0x00, %ch		/* load from the first cylinder */
	movb	$0x03, %cl		/* start from the third sector */
	mov	$__boot1, %bx		/* load sectors to the boot1 address */

	// call the bios routine
	int	$0x13

	// check if all went fine. If not, prepare for the next try
	jc	boot0.fail

	// push segment and offset to the stack, and jump to that location
	push	$0x0000			/* push the segment to the stack */
	push	$boot1			/* push the offset to the stack */
	lret				/* do a long return */

boot0.fail:
	// decrease the try counter
	dec	%di

	// if the try arrived to 0, there probably is something
	// wrong with our call, so we will abort
	jz	boot0.halt

	// jump back to do the next try
	jmp	boot0.load

boot0.halt:
	// disable interrupts and halt
	cli
	hlt

	// as we cleared interrupts, we should not arrive here. But if we do,
	// jump back and do an infinite loop
	jmp	boot0.halt

