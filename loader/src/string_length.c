
/* ----------------------------------------------------------------- *
 *
 *   string_length.c: provides a function to retrieve the
 *                    length of a NUL terminated string,
 *                    much like `strlen' from the C standard
 *                    library.
 *
 *   Copyright (C) 2017 Sandro Francesco Montemezzani
 *   Author: Sandro Montemezzani <sandro@montemezzani.net>
 *
 *   This file is part of the bootloader for the SFM-OS kernel,
 *   and is licensed under the MIT License.
 *
 * ----------------------------------------------------------------- */


#include <loader.h>


BOOT1_FUNC unsigned int string_length ( char str[] )
{
	// create the temporary pointer variables so
	// we don't have to modify the arguments
	char* s = (char*)( str );

	// create the local character counter variable
	unsigned int n = 0;

	// loop through the characters until we reached
	// a NUL byte
	while ( LIKELY( *s++ ) )
	{
		// increase `n'
		++n;
	}

	// return local character counter
	return n;
}

